/*********************************************************************
  @file
  Name:         v792.c
  Created by:   Pierre-Andre Amaudruz
  Contributors: Sergio Ballestrero

  Contents:     V792 32ch. QDC
                V785 32ch. Peak ADC include
                V792 and V785 are mostly identical;
                v792N_ identifies commands supported by both
                v792N_ identifies commands supported by V792 only
                v785_ identifies commands supported by V785 only
                
  $Log: v792.c,v $
*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <signal.h>

#if defined(OS_LINUX)
#include <unistd.h>
#endif

#include "v792n.h"
#include "mvmestd.h"
#include "v1190A.h"
#if defined(OS_RTEMS)
#include <rtems.h>
#endif

/*****************************************************************/
/*
Read single event, return event length (number of entries)
Uses single vme access! (1us/D32)
*/
int v792N_EventRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry)
{
  DWORD hdata; 
  int   cmode;
  
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
 
  *nentry = 0;
  //int counter=0;
  if (v792N_DataReady(mvme, base)) {
    do {
      hdata = mvme_read_value(mvme, base);
    //  printk(" hdata = 0x%x \n",hdata );
    } while (!(hdata & 0x02000000)); // skip up to the header
    
    pdest[*nentry] = hdata;
    *nentry += 1;
    do {
      pdest[*nentry] = mvme_read_value(mvme, base);
      *nentry += 1;
    } while (!(pdest[*nentry-1] & 0x04000000)); // copy until the trailer

    nentry--;
  }
  mvme_set_dmode(mvme, cmode);
  return *nentry;
}

/*****************************************************************/
/*
Read nentry of data from the data buffer. Will use the DMA engine
if size is larger then 127 bytes. 
*/
int v792N_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry)
{
  int  cmode;
  
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D32);
  *nentry = 128;
  if (v792N_DataReady(mvme, base)) {
    mvme_read(mvme, pdest, base, *nentry*4); 
  }
  mvme_set_dmode(mvme, cmode);
  return *nentry;
}

/*****************************************************************/
/**
 * Write Thresholds and read them back
 */
int v792N_ThresholdWrite(MVME_INTERFACE *mvme, DWORD base, WORD *threshold)
{
  int k, cmode;
  
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  for (k=0; k<V792N_MAX_CHANNELS ; k++) {
    mvme_write_value(mvme, base+V792N_THRES_BASE+4*k, threshold[k] & 0x1FF);
//	printf("Writing Threshold %d into 0x%08x for qdc(%d)\n",threshold[k],base+V792N_THRES_BASE+4*k,k); 
  }
  
  for (k=0; k<V792N_MAX_CHANNELS ; k++) {
    threshold[k] = mvme_read_value(mvme, base+V792N_THRES_BASE+4*k) & 0x1FF;
  }

  mvme_set_dmode(mvme, cmode);
  return V792N_MAX_CHANNELS;
}

/*****************************************************************/
/**
 * Read Thresholds
 */
int v792N_ThresholdRead(MVME_INTERFACE *mvme, DWORD base, WORD *threshold)
{
  int k, cmode;
  
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  
  for (k=0; k<V792N_MAX_CHANNELS ; k++) {
    threshold[k] = mvme_read_value(mvme, base+V792N_THRES_BASE+4*k) & 0x1FF;
  }
  mvme_set_dmode(mvme, cmode);
  return V792N_MAX_CHANNELS;
}

/*****************************************************************/
void v792N_EvtTriggerSet(MVME_INTERFACE *mvme, DWORD base, int count)
{
  int cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_EVTRIG_REG_RW, (count & 0x1F));
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_EvtCntRead(MVME_INTERFACE *mvme, DWORD base, DWORD *evtcnt)
{
  int cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  *evtcnt  = mvme_read_value(mvme, base+V792N_EVT_CNT_L_RO);
  *evtcnt += (mvme_read_value(mvme, base+V792N_EVT_CNT_H_RO) << 16);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
int v792N_CSR1Read(MVME_INTERFACE *mvme, DWORD base)
{
  int status, cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  status = mvme_read_value(mvme, base+V792N_CSR1_RO);
  mvme_set_dmode(mvme, cmode);
  return status;
}

/*****************************************************************/
int v792N_isBusy(MVME_INTERFACE *mvme, DWORD base)
{
  int status, busy, timeout, cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  timeout = 1000;
  do {
    status = mvme_read_value(mvme, base+V792N_CSR1_RO);
    busy = status & 0x4;
    timeout--;
  } while (busy || timeout);
  mvme_set_dmode(mvme, cmode);
  return (busy != 0 ? 1 : 0);
}

/*****************************************************************/
int v792N_CSR2Read(MVME_INTERFACE *mvme, DWORD base)
{
  int status, cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  status = mvme_read_value(mvme, base+V792N_CSR2_RO);
  mvme_set_dmode(mvme, cmode);
  return status;
}

/*****************************************************************/
int v792N_BitSet2Read(MVME_INTERFACE *mvme, DWORD base)
{
  int status, cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  status = mvme_read_value(mvme, base+V792N_BIT_SET2_RW);
  mvme_set_dmode(mvme, cmode);
  return status;
}

/*****************************************************************/
void v792N_BitSet2Set(MVME_INTERFACE *mvme, DWORD base, WORD pat)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_BIT_SET1_RW, 0xA0);
  mvme_write_value(mvme, base+V792N_BIT_CLEAR1_WO, 0xA0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_BitSet2Clear(MVME_INTERFACE *mvme, DWORD base, WORD pat)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_BIT_SET1_RW, 0xA0);
  mvme_write_value(mvme, base+V792N_BIT_CLEAR1_WO, 0xA0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_DataClear(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_BIT_SET2_RW, 0x4);
  mvme_write_value(mvme, base+V792N_BIT_CLEAR2_WO, 0x4);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_SoftReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_BIT_SET1_RW, 0xA0);
  mvme_write_value(mvme, base+V792N_BIT_CLEAR1_WO, 0xA0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_OnlineSet(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_BIT_CLEAR2_WO, 0x2);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_LowThEnable(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
//  mvme_write_value(mvme, base+V792N_BIT_SET2_RW, 0x10);
  mvme_write_value(mvme, base+V792N_BIT_CLEAR2_WO, 0x10);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_EmptyEnable(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_BIT_SET2_RW, 0x1000);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_EvtCntReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_EVT_CNT_RST_WO, 1);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_SingleShotReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_SINGLE_RST_WO, 1);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
int  v792N_DataReady(MVME_INTERFACE *mvme, DWORD base)
{
  int data_ready, cmode;

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  
  data_ready = mvme_read_value(mvme, base+V792N_CSR1_RO);
  /* printk("[DataReady] %08x \n",data_ready ); */
  mvme_set_dmode(mvme, cmode);
  return (data_ready & 0x1) && (data_ready != 0xffffffff);
}

/*****************************************************************/
int  v792N_isEvtReady(MVME_INTERFACE *mvme, DWORD base)
{
  int csr;
  csr = v792N_CSR1Read(mvme, base);
  return (csr & 0x100);  
}

/*****************************************************************/
void v792N_IntSet(MVME_INTERFACE *mvme, DWORD base, int level, int vector)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_INT_VECTOR_WO, (vector & 0xFF));
  mvme_write_value(mvme, base+V792N_INT_LEVEL_WO, (level & 0x7));
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_IntEnable(MVME_INTERFACE *mvme, DWORD base, int level)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_EVTRIG_REG_RW, (level & 0x1F));
  /* Use the trigger buffer for int enable/disable
  mvme_write_value(mvme, base+V792N_INT_LEVEL_WO, (level & 0x7));
  */
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v792N_IntDisable(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_EVTRIG_REG_RW, 0);
  /* Use the trigger buffer for int enable/disable
     Setting a level 0 reboot the VMIC !
  mvme_write_value(mvme, base+V792N_INT_LEVEL_WO, 0);
  */
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
/**
 * read Control Register 1 (0x1010,16 bit)
 */
WORD v792N_ControlRegister1Read(MVME_INTERFACE *mvme, DWORD base) {
  int cmode;
  WORD pat;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  pat = mvme_read_value(mvme, base+V792N_CR1_RW);
  mvme_set_dmode(mvme, cmode);
  return pat;
}

/*****************************************************************/
/**
 * write Control Register 1 (0x1010,16 bit)
 */
void v792N_ControlRegister1Write(MVME_INTERFACE *mvme, DWORD base, WORD pat) {
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_CR1_RW,pat);
  mvme_set_dmode(mvme, cmode);
}

void v792N_SetPedestal(MVME_INTERFACE *mvme, DWORD base, int pedestal){
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_IPED_RW,pedestal);
  mvme_set_dmode(mvme, cmode);
}




/*****************************************************************/
/**
Sets all the necessary paramters for a given configuration.
The configuration is provided by the mode argument.
Add your own configuration in the case statement. Let me know
your setting if you want to include it in the distribution.
@param *mvme VME structure
@param  base Module base address
@param mode  Configuration mode number
@param *nentry number of entries requested and returned.
@return MVME_SUCCESS
*/
int  v792N_Setup(MVME_INTERFACE *mvme, DWORD base, int mode)
{
  int  cmode;
  //int pedestal;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
 // WORD Thresholds[V792N_MAX_CHANNELS];
 // int Threshold=0x9;
 // int thresholdcount=0;
//	pedestal=0xff;
  switch (mode) {
  case 1:
    printf("Default setting after power up (mode:%d)\n", mode);
    printf("\n");
    break;
  case 2:
    printf("Modified setting (mode:%d)\n", mode);
//    for(thresholdcount=0;thresholdcount<V792N_MAX_CHANNELS;thresholdcount++)Thresholds[thresholdcount]=Threshold;
    printf("Empty Enable, Over Range disable, Low Th Enable\n");
    v792N_OnlineSet(mvme, base); //clear bit set 2 register, offline bit. zero means online
    //v792N_EmptyEnable(mvme, base);
    v792N_LowThEnable(mvme, base);
    //v792N_ThresholdWrite(mvme,base,Thresholds);
  //  v792N_SetPedestal(mvme,base,pedestal);
//v792N_BitSet2Set(mvme, base, 0x10);
	
//    printf("Thresholds after are : %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d \n",Thresholds[0], Thresholds[1], Thresholds[2], Thresholds[3], Thresholds[4], Thresholds[5], Thresholds[6], Thresholds[7], Thresholds[8], Thresholds[9], Thresholds[10], Thresholds[11], Thresholds[12], Thresholds[13], Thresholds[14], Thresholds[15]);
    break;
  default:
    printf("Unknown setup mode\n");
    mvme_set_dmode(mvme, cmode);
    return -1;
  }
  mvme_set_dmode(mvme, cmode);
  return 0;
}

/*****************************************************************/
void  v792N_Status(MVME_INTERFACE *mvme, DWORD base)
{
  int status, cmode, i;
  WORD threshold[V792N_MAX_CHANNELS];

  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  printf("v792 Status for Base:%x\n", base);
  status = mvme_read_value(mvme, base+V792N_FIRM_REV);
  printf("Firmware revision: 0x%x\n", status);
  status = v792N_CSR1Read(mvme, base);
  udelay(100000000);

  printf("DataReady    :%s\t", status & 0x1 ? "Y" : "N");
  printf(" - Global Dready:%s\t", status & 0x2 ? "Y" : "N");
  printf(" - Busy         :%s\n", status & 0x4 ? "Y" : "N");
  printf("Global Busy  :%s\t", status & 0x8 ? "Y" : "N");
  printf(" - Amnesia      :%s\t", status & 0x10 ? "Y" : "N");
  printf(" - Purge        :%s\n", status & 0x20 ? "Y" : "N");
  printf("Term ON      :%s\t", status & 0x40 ? "Y" : "N");
  printf(" - TermOFF      :%s\t", status & 0x80 ? "Y" : "N");
  printf(" - Event Ready  :%s\n", status & 0x100 ? "Y" : "N");
  status = v792N_CSR2Read(mvme, base);
  printf("Buffer Empty :%s\t", status & 0x2 ? "Y" : "N");
  printf(" - Buffer Full  :%s\t", status & 0x4 ? "Y" : "N");
  printf(" - Csel0        :%s\n", status & 0x10 ? "Y" : "N");
  printf("Csel1        :%s\t", status & 0x20 ? "Y" : "N");
  printf(" - Dsel0        :%s\t", status & 0x40 ? "Y" : "N");
  printf(" - Dsel1        :%s\n", status & 0x80 ? "Y" : "N");
  status = v792N_BitSet2Read(mvme, base);
  udelay(100000000);
  printf("Test Mem     :%s\t", status & 0x1 ? "Y" : "N");
  printf(" - Offline      :%s\t", status & 0x2 ? "Y" : "N");
  printf(" - Clear Data   :%s\n", status & 0x4  ? "Y" : "N");
  printf("Over Range En:%s\t", status & 0x8  ? "Y" : "N");
  printf(" - Low Thres En :%s\t", status & 0x10 ? "Y" : "N");
  printf(" - Auto Incr    :%s\n", status & 0x20 ? "Y" : "N");
  printf("Empty Enable :%s\t", status & 0x1000 ? "Y" : "N");
  printf(" - Slide sub En :%s\t", status & 0x2000 ? "Y" : "N");
  printf(" - All Triggers :%s\n", status & 0x4000 ? "Y" : "N");
  v792N_ThresholdRead(mvme, base, threshold);
  for (i=0;i<V792N_MAX_CHANNELS;i+=2) {
    printf("Threshold[%i] = 0x%4.4x\t   -  ", i, threshold[i]);
    printf("Threshold[%i] = 0x%4.4x\n", i+1, threshold[i+1]);
  }
  mvme_set_dmode(mvme, cmode);
}


//** set bit 0 of BitSetRegister2 (0x1032) to 1 section 4.26
//** write word into memory Test Address Register (0x1036) section 4.28
//** write high and low part of 32 bit test word in Testword_high (0x1038)
//   and testword_low (0x103a). sections 4.29 and 4.30
//** Wire the RTestaddress register  (0x1064) section 4.35 
void v792N_SetTestRandom(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V792N_EVTRIG_REG_RW, 0);
  mvme_set_dmode(mvme, cmode);

}

void v792N_SetTestAcquisition(MVME_INTERFACE *mvme, DWORD base,WORD value)
{
  int cmode;
  int i;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
	// select acquisition test mode 
	// and reset write pointer in fifo
  v792N_BitSet2Set(mvme,base,0x40); //1<<6 = 0x40
	// reset read pointer in fifo and release write pointer
  v792N_BitSet2Clear(mvme,base,0x40); //1<<6 = 0x40
	//write 32 data words to adc converted values
  for(i=0;i<32;i++){
		//set data to a nice middle spike i.e channel 2048 = 0x800
		//into test write register
  	mvme_write_value(mvme, base+V792N_TEST_EVENT_WO,value); //pull this out of odb ?
  }
	//reset the write pointer and release the read pointer
  v792N_BitSet2Set(mvme,base,0x40);
	// we are now ready to acquire fake data.
  mvme_write_value(mvme, base+V792N_EVTRIG_REG_RW, 0);
  mvme_set_dmode(mvme, cmode);
}
/*****************************************************************/
/**
 * decoded printout of readout entry
 * Not to be trusted for data decoding but acceptable for display
 * purpose as its implementation is strongly compiler dependent and
 * not flawless. 
 * @param v
 */
void v792N_printEntry(const v792N_Data* v) {
  switch (v->data.type) {
  case v792N_typeMeasurement:
    printf("Data=0x%08x Measurement ch=%3d v=%6d over=%1d under=%1d\n",
	   v->raw,v->data.channel,v->data.adc,v->data.ov,v->data.un);
    break;
  case v792N_typeHeader:
    printf("Data=0x%08x Header geo=%2x crate=%2x cnt=%2d\n",
    	   v->raw,v->header.geo,v->header.crate,v->header.cnt);
    break;
  case v792N_typeFooter:
    printf("Data=0x%08x Footer geo=%2x evtCnt=%7d\n",
    	   v->raw,v->footer.geo,v->footer.evtCnt);
    break;
  case v792N_typeFiller:
    printf("Data=0x%08x Filler\n",v->raw);
    break;
  default:
    printf("Data=0x%08x Unknown %04x\n",v->raw,v->data.type);
    break;
  }
}

/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {

  DWORD VMEIO_BASE = 0x780000;
  DWORD V792N_BASE  = 0x500000;
  
  MVME_INTERFACE *myvme;

  int status, csr, i;
  DWORD cnt;
  DWORD dest[1000];
  WORD  threshold[32];

  if (argc>1) {
    sscanf(argv[1],"%lx",&V792N_BASE);
  }

  // Test under vmic   
  status = mvme_open(&myvme, 0);

  // Set am to A24 non-privileged Data
  mvme_set_am(myvme, MVME_AM_A24_ND);

  // Set dmode to D32
  mvme_set_dmode(myvme, MVME_DMODE_D32);

  v792N_SingleShotReset(myvme, V792N_BASE);

  for (i=0;i<V792N_MAX_CHANNELS;i++) {
    threshold[i] = 0;
  }
  v792N_ThresholdWrite(myvme, V792N_BASE, threshold);
  v792N_DataClear(myvme, V792N_BASE);
  v792N_Setup(myvme, V792N_BASE, 2);

  v792N_Status(myvme, V792N_BASE);

  while (1) {
    do {
      csr = v792N_CSR1Read(myvme, V792N_BASE);
      printf("CSR1: 0x%x\n", csr);
      csr = v792N_CSR2Read(myvme, V792N_BASE);
      printf("CSR2: 0x%x\n", csr);
      printf("Busy : %d\n", v792N_isBusy(myvme, V792N_BASE));
#if defined(OS_LINUX)
      sleep(1);
#endif
    } while (csr == 0);
 
    // Read Event Counter
    v792N_EvtCntRead(myvme, V792N_BASE, &cnt);
    printf("Event counter: 0x%lx\n", cnt);

    // Set 0x3 in pulse mode for timing purpose
    mvme_write_value(myvme, VMEIO_BASE+0x8, 0xF); 

    // Write pulse for timing purpose
    mvme_write_value(myvme, VMEIO_BASE+0xc, 0x2);

    //  cnt=32;
    //  v792N_DataRead(myvme, V792N_BASE, dest, &cnt);

    // Write pulse for timing purpose
    mvme_write_value(myvme, VMEIO_BASE+0xc, 0x8);

    //  for (i=0;i<32;i++) {
    //    printf("Data[%i]=0x%x\n", i, dest[i]);
    //  }
  
    //  status = 32;
    v792N_EventRead(myvme, V792N_BASE, dest, &status);
    printf("count: 0x%x\n", status);
    for (i=0;i<status;i++) {
      printf("%d)",i);
      v792N_printEntry((v792N_Data*)&dest[i]);
    }
  }
  status = mvme_close(myvme);
  return 1;
}
#endif

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
