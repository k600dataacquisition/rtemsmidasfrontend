/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Thu Aug 27 16:05:45 2009

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      comment[80];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"Comment = STRING : [80] PR137: (p,t) at zero degrees",\
"",\
NULL }

#ifndef EXCL_GLOBAL

#define GLOBAL_PARAM_DEFINED

typedef struct {
  float     misswires;
  float     z_x1x2;
  float     x_x1x2;
  float     min_x_wires;
  float     min_u_wires;
  float     max_u_wires;
  float     max_x_wires;
  float     lut_x1_offset;
  float     lut_x2_offset;
  float     lut_u2_offset;
  float     lut_u1_offset;
  float     x1_1st_wire_chan;
  float     x2_1st_wire_chan;
  float     u1_1st_wire_chan;
  float     u2_1st_wire_chan;
  float     x1_last_wire_chan;
  float     x2_last_wire_chan;
  float     u1_last_wire_chan;
  float     u2_last_wire_chan;
} GLOBAL_PARAM;

#define GLOBAL_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"misswires = FLOAT : 2",\
"z_x1x2 = FLOAT : 295",\
"x_x1x2 = FLOAT : 391",\
"min_x_wires = FLOAT : 3",\
"min_u_wires = FLOAT : 3",\
"max_u_wires = FLOAT : 8",\
"max_x_wires = FLOAT : 8",\
"lut_x1_offset = FLOAT : 160",\
"lut_x2_offset = FLOAT : 80",\
"lut_u2_offset = FLOAT : 65",\
"lut_u1_offset = FLOAT : 50",\
"x1_1st_wire_chan = FLOAT : 10",\
"x2_1st_wire_chan = FLOAT : 510",\
"u1_1st_wire_chan = FLOAT : 301",\
"u2_1st_wire_chan = FLOAT : 801",\
"x1_last_wire_chan = FLOAT : 208",\
"x2_last_wire_chan = FLOAT : 708",\
"u1_last_wire_chan = FLOAT : 443",\
"u2_last_wire_chan = FLOAT : 943",\
"",\
NULL }

#endif

#ifndef EXCL_GATES

#define GATES_PARAM_DEFINED

typedef struct {
  float     x1_driftt_low;
  float     x1_driftt_hi;
  float     x2_driftt_low;
  float     x2_driftt_hi;
  float     u2_driftt_low;
  float     u2_driftt_hi;
  float     u1_driftt_low;
  float     u1_driftt_hi;
  float     lowtof;
  float     hitof;
  float     lowpad1;
  float     lowpad2;
  float     hipad1;
  float     hipad2;
  float     thetafp_lo;
  float     thetafp_hi;
  float     y_lo;
  float     y_hi;
} GATES_PARAM;

#define GATES_PARAM_STR(_name) char *_name[] = {\
"[.]",\
"x1_driftt_low = FLOAT : 5500",\
"x1_driftt_hi = FLOAT : 7500",\
"x2_driftt_low = FLOAT : 5500",\
"x2_driftt_hi = FLOAT : 7500",\
"u2_driftt_low = FLOAT : 5500",\
"u2_driftt_hi = FLOAT : 7500",\
"u1_driftt_low = FLOAT : 5700",\
"u1_driftt_hi = FLOAT : 7500",\
"lowtof = FLOAT : 5000",\
"hitof = FLOAT : 5600",\
"lowpad1 = FLOAT : 150",\
"lowpad2 = FLOAT : 160",\
"hipad1 = FLOAT : 1500",\
"hipad2 = FLOAT : 1500",\
"thetafp_lo = FLOAT : 34",\
"thetafp_hi = FLOAT : 38",\
"y_lo = FLOAT : -15",\
"y_hi = FLOAT : 15",\
"",\
NULL }

#endif

#ifndef EXCL_WIRECHAMBER

#define ASUM_BANK_DEFINED

typedef struct {
  float     sum;
  float     average;
} ASUM_BANK;

#define ASUM_BANK_STR(_name) char *_name[] = {\
"[.]",\
"Sum = FLOAT : 0",\
"Average = FLOAT : 0",\
"",\
NULL }

#define WIRECHAMBER_SETTINGS_DEFINED

typedef struct {
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_0;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_1;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_2;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_3;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_4;
  struct {
    INT       thresholds[16];
  } v792n_0;
  struct {
    struct {
      float     adc_threshold;
      float     misswires;
      float     max_tdc_channels;
      float     z_x1x2;
      float     x_x1x2;
      float     min_x_wires;
      float     min_u_wires;
      float     max_x_wires;
      float     max_u_wires;
      float     lut_x1_offset;
      float     lut_u1_offset;
      float     lut_x2_offset;
      float     lut_u2_offset;
      float     x1_1st_wire_chan;
      float     x1_last_wire_chan;
      float     x2_1st_wire_chan;
      float     x2_last_wire_chan;
      float     u1_1st_wire_chan;
      float     u1_last_wire_chan;
      float     u2_1st_wire_chan;
      float     u2_last_wire_chan;
    } global;
    struct {
      float     x1_driftt_low;
      float     x1_driftt_hi;
      float     x2_driftt_low;
      float     u1_driftt_low;
      float     u2_driftt_low;
      float     x2_driftt_hi;
      float     u2_driftt_hi;
      float     u1_driftt_hi;
      float     lowtof;
      float     hitof;
      float     lowpad1;
      float     lowpad2;
      float     hipad1;
      float     hipad2;
    } focalplane;
  } chambers;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_6;
  struct {
    DWORD     baseaddress;
    char      moduletype[7];
    BOOL      triggermatching;
    BOOL      keep_token;
    BOOL      setautoloaduserconfig;
    INT       windowwidth;
    INT       windowoffset;
    INT       extrasearchmargin;
    INT       rejectmargin;
    BOOL      triggertimesubtraction;
    short     edgedetection;
    short     leastsignificantbit;
    short     resolutionleadingwidth;
    short     channeldeadtime;
    BOOL      headertrailer;
    WORD      maximumhitsperevent;
    BOOL      errormark;
    BOOL      errorbypass;
    INT       tdcinteralerror;
    WORD      effectivereadoutfifosize;
    DWORD     channelenableupper;
    DWORD     channelenablelower;
    WORD      globaloffset;
    WORD      channeloffset[128];
    WORD      spare;
    WORD      testmode;
    short     controlregister;
    short     statusregister;
    BOOL      readregisterstoggle;
  } v1190a_5;
} WIRECHAMBER_SETTINGS;

#define WIRECHAMBER_SETTINGS_STR(_name) char *_name[] = {\
"[V1190A_0]",\
"BaseAddress = DWORD : 262144",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_1]",\
"BaseAddress = DWORD : 327680",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_2]",\
"BaseAddress = DWORD : 393216",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_3]",\
"BaseAddress = DWORD : 458752",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_4]",\
"BaseAddress = DWORD : 524288",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V792N_0]",\
"Thresholds = INT[16] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 511",\
"[8] 511",\
"[9] 511",\
"[10] 511",\
"[11] 511",\
"[12] 511",\
"[13] 511",\
"[14] 511",\
"[15] 511",\
"",\
"[chambers/GLOBAL]",\
"ADC Threshold = FLOAT : 0",\
"misswires = FLOAT : 0",\
"max_tdc_channels = FLOAT : 0",\
"z_x1x2 = FLOAT : 0",\
"x_x1x2 = FLOAT : 0",\
"min_x_wires = FLOAT : 0",\
"min_u_wires = FLOAT : 0",\
"max_x_wires = FLOAT : 0",\
"max_u_wires = FLOAT : 0",\
"lut_x1_offset = FLOAT : 0",\
"lut_u1_offset = FLOAT : 0",\
"lut_x2_offset = FLOAT : 0",\
"lut_u2_offset = FLOAT : 0",\
"x1_1st_wire_chan = FLOAT : 0",\
"x1_last_wire_chan = FLOAT : 0",\
"x2_1st_wire_chan = FLOAT : 0",\
"x2_last_wire_chan = FLOAT : 0",\
"u1_1st_wire_chan = FLOAT : 0",\
"u1_last_wire_chan = FLOAT : 0",\
"u2_1st_wire_chan = FLOAT : 0",\
"u2_last_wire_chan = FLOAT : 0",\
"",\
"[chambers/focalplane]",\
"x1_driftt_low = FLOAT : 0",\
"x1_driftt_hi = FLOAT : 0",\
"x2_driftt_low = FLOAT : 0",\
"u1_driftt_low = FLOAT : 0",\
"u2_driftt_low = FLOAT : 0",\
"x2_driftt_hi = FLOAT : 0",\
"u2_driftt_hi = FLOAT : 0",\
"u1_driftt_hi = FLOAT : 0",\
"lowtof = FLOAT : 0",\
"hitof = FLOAT : 0",\
"lowpad1 = FLOAT : 0",\
"lowpad2 = FLOAT : 0",\
"hipad1 = FLOAT : 0",\
"hipad2 = FLOAT : 0",\
"",\
"[V1190A_6]",\
"BaseAddress = DWORD : 0",\
"ModuleType = STRING : [7] ",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
"[V1190A_5]",\
"BaseAddress = DWORD : 589824",\
"ModuleType = STRING : [7] 7",\
"TriggerMatching = BOOL : n",\
"Keep_Token = BOOL : n",\
"SetAutoLoadUserConfig = BOOL : n",\
"WindowWidth = INT : 0",\
"WindowOffset = INT : 0",\
"ExtraSearchMargin = INT : 0",\
"RejectMargin = INT : 0",\
"TriggerTimeSubtraction = BOOL : n",\
"EdgeDetection = SHORT : 0",\
"LeastSignificantBit = SHORT : 0",\
"ResolutionLeadingWidth = SHORT : 0",\
"ChannelDeadTime = SHORT : 0",\
"HeaderTrailer = BOOL : n",\
"MaximumHitsPerEvent = WORD : 0",\
"ErrorMark = BOOL : n",\
"ErrorBypass = BOOL : n",\
"TDCInteralError = INT : 0",\
"EffectiveReadoutFIFOSize = WORD : 0",\
"ChannelEnableUpper = DWORD : 0",\
"ChannelEnableLower = DWORD : 0",\
"GlobalOffset = WORD : 0",\
"ChannelOffset = WORD[128] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 0",\
"[20] 0",\
"[21] 0",\
"[22] 0",\
"[23] 0",\
"[24] 0",\
"[25] 0",\
"[26] 0",\
"[27] 0",\
"[28] 0",\
"[29] 0",\
"[30] 0",\
"[31] 0",\
"[32] 0",\
"[33] 0",\
"[34] 0",\
"[35] 0",\
"[36] 0",\
"[37] 0",\
"[38] 0",\
"[39] 0",\
"[40] 0",\
"[41] 0",\
"[42] 0",\
"[43] 0",\
"[44] 0",\
"[45] 0",\
"[46] 0",\
"[47] 0",\
"[48] 0",\
"[49] 0",\
"[50] 0",\
"[51] 0",\
"[52] 0",\
"[53] 0",\
"[54] 0",\
"[55] 0",\
"[56] 0",\
"[57] 0",\
"[58] 0",\
"[59] 0",\
"[60] 0",\
"[61] 0",\
"[62] 0",\
"[63] 0",\
"[64] 0",\
"[65] 0",\
"[66] 0",\
"[67] 0",\
"[68] 0",\
"[69] 0",\
"[70] 0",\
"[71] 0",\
"[72] 0",\
"[73] 0",\
"[74] 0",\
"[75] 0",\
"[76] 0",\
"[77] 0",\
"[78] 0",\
"[79] 0",\
"[80] 0",\
"[81] 0",\
"[82] 0",\
"[83] 0",\
"[84] 0",\
"[85] 0",\
"[86] 0",\
"[87] 0",\
"[88] 0",\
"[89] 0",\
"[90] 0",\
"[91] 0",\
"[92] 0",\
"[93] 0",\
"[94] 0",\
"[95] 0",\
"[96] 0",\
"[97] 0",\
"[98] 0",\
"[99] 0",\
"[100] 0",\
"[101] 0",\
"[102] 0",\
"[103] 0",\
"[104] 0",\
"[105] 0",\
"[106] 0",\
"[107] 0",\
"[108] 0",\
"[109] 0",\
"[110] 0",\
"[111] 0",\
"[112] 0",\
"[113] 0",\
"[114] 0",\
"[115] 0",\
"[116] 0",\
"[117] 0",\
"[118] 0",\
"[119] 0",\
"[120] 0",\
"[121] 0",\
"[122] 0",\
"[123] 0",\
"[124] 0",\
"[125] 0",\
"[126] 0",\
"[127] 0",\
"Spare = WORD : 0",\
"TestMode = WORD : 0",\
"controlregister = SHORT : 0",\
"statusregister = SHORT : 0",\
"readregisterstoggle = BOOL : n",\
"",\
NULL }

#define WIRECHAMBER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} WIRECHAMBER_COMMON;

#define WIRECHAMBER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 16777215",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 257",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] concurrentvme",\
"Frontend name = STRING : [32] K600frontend",\
"Frontend file name = STRING : [256] k600frontend.c",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 33",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 377",\
"Period = INT : 1000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] concurrentvme",\
"Frontend name = STRING : [32] K600frontend",\
"Frontend file name = STRING : [256] k600frontend.c",\
"",\
NULL }

#define SCALER_SETTINGS_DEFINED

typedef struct {
  char      names[18][32];
} SCALER_SETTINGS;

#define SCALER_SETTINGS_STR(_name) char *_name[] = {\
"[.]",\
"Names = STRING[18] :",\
"[32] Trigger",\
"[32] Pulser",\
"[32] Current Intgr",\
"[32] Clock",\
"[32] BeamLoss1",\
"[32] BeamLoss2",\
"[32] BeamLoss3",\
"[32] BeamLoss4",\
"[32] BeamLoss5",\
"[32] Trigger-I",\
"[32] Pulser-I",\
"[32] Current Intgr-I",\
"[32] Clock-I",\
"[32] BeamLoss1-I",\
"[32] BeamLoss2-I",\
"[32] BeamLoss3-I",\
"[32] BeamLoss4-I",\
"[32] BeamLoss5-I",\
"",\
NULL }

#endif

#ifndef EXCL_BEAMLINE

#define BEAMLINE_EVENT_DEFINED

typedef struct {
  float     demand[20];
  float     measured[20];
} BEAMLINE_EVENT;

#define BEAMLINE_EVENT_STR(_name) char *_name[] = {\
"[.]",\
"Demand = FLOAT[20] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] 0",\
"[16] 0",\
"[17] 0",\
"[18] 0",\
"[19] 35",\
"Measured = FLOAT[20] :",\
"[0] 191",\
"[1] 0.01068132",\
"[2] 0.06599527",\
"[3] 0",\
"[4] 0.1846342",\
"[5] 0.09765774",\
"[6] 0.2685588",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"[10] 0",\
"[11] 0",\
"[12] 0",\
"[13] 0",\
"[14] 0",\
"[15] -621.1",\
"[16] -341.675",\
"[17] 337",\
"[18] 435",\
"[19] 14",\
"",\
NULL }

#define BEAMLINE_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} BEAMLINE_COMMON;

#define BEAMLINE_COMMON_STR(_name) char *_name[] = {\
"[.]",\
"Event ID = WORD : 3",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 16",\
"Source = INT : 0",\
"Format = STRING : [8] FIXED",\
"Enabled = BOOL : y",\
"Read on = INT : 121",\
"Period = INT : 60000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 1",\
"Frontend host = STRING : [32] xiafe",\
"Frontend name = STRING : [32] k600Epics",\
"Frontend file name = STRING : [256] frontend.c",\
"",\
NULL }

#define BEAMLINE_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      channel_name[20][32];
    } beamline;
  } devices;
  char      names[20][32];
  float     update_threshold_measured[20];
} BEAMLINE_SETTINGS;

#define BEAMLINE_SETTINGS_STR(_name) char *_name[] = {\
"[Devices/Beamline]",\
"Channel name = STRING[20] :",\
"[32] VarTable:FC10J:ActValue",\
"[32] VarTable:hex01spi:ActValue",\
"[32] VarTable:quad01sp:ActValue",\
"[32] VarTable:bmag01sp:ActValue",\
"[32] VarTable:trim01sp:ActValue",\
"[32] VarTable:bmag02sp:ActValue",\
"[32] VarTable:trim02sp:ActValue",\
"[32] VarTable:quad01si:ActValue",\
"[32] VarTable:quad02si:ActValue",\
"[32] VarTable:quad03si:ActValue",\
"[32] VarTable:quad04si:ActValue",\
"[32] VarTable:quad05si:ActValue",\
"[32] VarTable:quad06si:ActValue",\
"[32] VarTable:PageQMch:ActValue",\
"[32] VarTable:spilock:ActValue",\
"[32] VarTable:spqd1Lo:ActValue",\
"[32] VarTable:spqd1Up:ActValue",\
"[32] VarTable:spbm1Lo:ActValue",\
"[32] VarTable:spbm2Up:ActValue",\
"[32] Watchdog",\
"",\
"[.]",\
"Names = STRING[20] :",\
"[32] Faraday Cup 10",\
"[32] Hexapole 1 SP",\
"[32] Quad 1 SP Line",\
"[32] Bending Magnet 1 SP",\
"[32] Trim Coil H SP",\
"[32] Bending Magnet 2 SP",\
"[32] Trim Coil K SP",\
"[32] Quad 1 S Line",\
"[32] Quad 2 S Line",\
"[32] Quad 3 S Line",\
"[32] Quad 4 S Line",\
"[32] Quad 5 S Line",\
"[32] Quad 6 S Line",\
"[32] Match S Quads/Spctrm",\
"[32] SP Interlock Control",\
"[32] SP Quad1 Low Limit",\
"[32] Default%CH 17",\
"[32] Default%CH 18",\
"[32] Default%CH 19",\
"[32] Watchdog",\
"Update Threshold Measured = FLOAT[20] :",\
"[0] 1",\
"[1] 1",\
"[2] 1",\
"[3] 1",\
"[4] 1",\
"[5] 1",\
"[6] 1",\
"[7] 1",\
"[8] 1",\
"[9] 1",\
"[10] 1",\
"[11] 1",\
"[12] 1",\
"[13] 1",\
"[14] 1",\
"[15] 1",\
"[16] 1",\
"[17] 1",\
"[18] 1",\
"[19] 1",\
"",\
NULL }

#endif

