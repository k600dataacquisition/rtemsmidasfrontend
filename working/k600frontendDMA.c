/*
 * =====================================================================================
 *
 *       Filename:  k600frontendDMA.c
 *
 *    Description:  DMA version of the k600frontend.c
 *
 *        Version:  1.0
 *        Created:  07/09/2009 14:54:25
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

/* #####   HEADER FILE INCLUDES   ######################################## */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>

/* RTEMS NETWORK */
#include "rtemsnetwork.h"

/* TIMING */
#include <rtems/rtems/timer.h>

/* #####   MIDAS HEADER FILE INCLUDES   ################################### */
#include "midas.h"
#include "mcstd.h"
#include "experim.h"
#include "mvmestd.h"
#include "msystem.h"

/* #####   MODULES HEADER FILE INCLUDES  ################################## */
#include "v1190A.h"	// TDC
#include "v830.h"       // Scalar
#include "v792n.h"	// QDC


/* MIDAS VME INTERFACE */
#include "rtemsVME.h"

/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE  ########################## */
/* make frontend functions callable from the C framework OPEN */
#ifdef __cplusplus
	extern "C" {
#endif

/*-----------------------------------------------------------------------------
 *    Debug and Signal handlers
 *-----------------------------------------------------------------------------*/
//#define DEBUG 1
//#define DEBUG2 1
//#define DEBUGMESSAGE 1
//#define DEBUGDATA 1
//#define HDL_SIGNALS	

		
/*-----------------------------------------------------------------------------
 *   Thread Implementation   
 *-----------------------------------------------------------------------------*/
#define MAX_CTHREADS 1 		// the max number of threads we want to use

/* maximum event size produced by this frontend */
#define USER_MAX_EVENT_SIZE (4*1024*1024)       // 2 MB

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/* ring buffer handle */
static int rbh_v1190[8]; 			// all v1190s
static int rbh_v792n; 				// v792n
static int rbh_sync[MAX_CTHREADS];       	// sync threads -> collecting
static int rbh_coll;         			// collecting -> main

extern int rbh1, rbh2, rbh1_next;
   
volatile int currentThread;
volatile int stop_thread = 0;
volatile void* startringpointer;
static int running;    				// thread running


typedef struct {
	pid_t threadpid;
	int active;
	midas_thread_t thread_id;
	int threadnumber;
	void *threadcache;
}cthread;

typedef struct {
	midas_thread_t thread_id;
	char *pevent;
	char *message;
}thread_data;


static cthread threadstack[MAX_CTHREADS]; 	// a stack of threads
static pid_t parentthread_pid;
static int numThreads;	 			// how many threads did i start


volatile char* pevent_threaddata;
volatile int DataReady_threaddata;
volatile int count_threaddata;
volatile int setupdone = 0;	
/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ###################### */

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "K600frontend";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 3000;

/* maximum event size produced by this frontend */
INT max_event_size = 1000000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 1000000;

/*-----------------------------------------------------------------------------
 *  VME Hardware Specific Variables 
 *-----------------------------------------------------------------------------*/
MVME_INTERFACE *k600vme;

int V792_BASE;
int V259_BASE;
int V1190_BASE[7];
int V830_BASE[2];
int MaxScalers=2;
const int V1190Count=3;//N_TDC_MOD;

/* number of channels */
#define N_ADC  16
//#define N_TDC  768
//#define N_TDC  896
#define MAXINPUTSCALERS 9
int N_TDC;

const unsigned int N_TDC_MOD= 7;
#define N_SCLR 64
#define N_PAT 1
	
const int kDataSize = 100000;
DWORD tdcdata[1000000];

DWORD  storedeventcounter[7];
DWORD  eventcounter[7];
DWORD qdceventcounter;
DWORD avgtdctime,avgqdctime,avgpattime;
DWORD prevscalercount[3][MAXINPUTSCALERS];

char *tempbuffer;


//unsigned short Channel2Wire[N_TDC];
unsigned short *Channel2Wire;

extern HNDLE hDB;
HNDLE hSet;
WIRECHAMBER_SETTINGS K600TriggerSettings;

/* #####   PROTOTYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */
/*-----------------------------------------------------------------------------
 *  Midas Frontend Function Prototypes
 *-----------------------------------------------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

/*-----------------------------------------------------------------------------
 *  Equipment List Function Prototypes
 *-----------------------------------------------------------------------------*/
INT read_wirechamber_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

/*-----------------------------------------------------------------------------
 *  Module Read Out Function Prototypes
 *-----------------------------------------------------------------------------*/
int read_v259(const char*bank_name,char*pevent);
int read_tdc(char *pevent);
int read_v792(int base,const char*bank_name,char*pevent,int number_of_channels);

/*-----------------------------------------------------------------------------
 *  Miscelaneous functions
 *-----------------------------------------------------------------------------*/
int MapChannel2Wire(int channel_number);
void wirechamber_settings_callback(INT hDB, INT hseq, void *info);

/*-----------------------------------------------------------------------------
 *   Thread Implementation Prototypes
 *-----------------------------------------------------------------------------*/
	void* readout_threadfunc(void* arg);
	INT init_ringbuffer();
	INT delete_ringbuffer();
	INT stop_threads(); 
	INT start_thread( );
	INT init_thread();

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */

/*-----------------------------------------------------------------------------
 *    Equipment list 
 *-----------------------------------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {
   {"WireChamber",                /* equipment name */
    {1, 0,                   	  /* event ID, trigger mask */
     "K600SYSTEMS",               /* event buffer */
#ifdef USE_INT
     EQ_INTERRUPT,          	  /* equipment type */
#else

#ifdef TEST_THREADS
     EQ_MULTITHREAD,              /* equipment type */
#else
     EQ_POLLED,                   /* equipment type */
#endif


#endif
     LAM_SOURCE(0, 0xFFFFFF),     /* event source crate 0, all stations */
     "MIDAS",                     /* format */
     TRUE,                        /* enabled */
     RO_RUNNING |		  /* read only when running */
     RO_ODB,                      /* and update ODB */
     250,                         /* poll for 250ms */
     0,                           /* stop run after this event limit */
     0,                           /* number of sub events */
     0,                           /* don't log history */
     "concurrentvme", /* host on which front is running*/	
     "k600frontend", 		  /* frontend name*/
     "k600frontend.c",},  /* source file used for user FE  */	
    read_wirechamber_event,       /* readout routine*/
    },

   {"Scaler",                     /* equipment name */
    {2, 0,                        /* event ID, trigger mask */
     "K600SYSTEMSCALER",          /* event buffer */
     EQ_PERIODIC | EQ_MANUAL_TRIG,/* equipment type */
     0,                           /* event source */
     "MIDAS",                     /* format */
     TRUE,                        /* enabled */
     RO_RUNNING | RO_TRANSITIONS |/* read when running and on transitions */
     RO_ODB,                      /* and update ODB */
     5000,                        /* read every 5 sec */
     0,                           /* stop run after this event limit */
     0,                           /* number of sub events */
     1,                           /* log history */
     "concurrentvme", /*host on which the front end is running*/
     "k600frontend",              /*front end name*/
     "k600frontend.c",}, /* source file used for user FE */
    read_scaler_event,            /* readout routine */
    },

   {""}
};

/* make frontend functions callable from the C framework CLOSED */
#ifdef __cplusplus
	}
#endif


/* #################### RTEMS INIT ################################ */

extern INT mfe(char *ahost_name, char *aexp_name, BOOL adebug);


/*
 * RTEMS Startup Task
 */
rtems_task
Init (rtems_task_argument ignored)
{

  printk( "Initialize network\n" );
  rtems_bsdnet_initialize_network ();
  printk( "Network initialized\n" );
  rtems_bsdnet_show_inet_routes ();
 
  int numberofdriver = libbsdport_netdriver_dump(NULL);

 
  
  printk( "Initiating Frontend\n" );
  mfe("xiafe","k600",0 );
  exit (0);
}

/* ##################### RTEMS INIT ################################ */

/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ############ */
/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  frontend_init
 *  Description :   When the frontend program is started. 
 *  		    This routine should initialize the hardware.
 *  Parameters  :   Void
 *  Returns     :   SUCCESS,FE_ERR_HW
 * =====================================================================================
 */
INT frontend_init()
{
	int status=0;
	int counter=0;
	char set_str[80];
	int size;
	int tdcmod;
	HNDLE hkey;
	
	//print message and return FE_ERR_HW if frontend should not be started 
	printf("Frontend Testing Access to ODB for setup procedures.\n");
	

	/* cm_msg(MINFO,"k600frontend","Frontend Testing Access to ODB for setup procedures."); */
		

	WIRECHAMBER_SETTINGS_STR(wirechamber_settings_str);

	//Map /equipment/Trigger/settings for k600 
  	sprintf(set_str, "/Equipment/WireChamber/Settings");
  	status = db_create_record(hDB, 0, set_str, strcomb(wirechamber_settings_str));
  	
	printf("\n status db_create_record = %d \n ",status);
	status = db_find_key (hDB, 0, set_str, &hSet);
  	if (status != DB_SUCCESS)
    		cm_msg(MINFO,"FE","Key %s not found", set_str);
	
       	//Enable hot-link on settings/ of the equipment
  	size = sizeof(WIRECHAMBER_SETTINGS);
  	if ((status = db_open_record(hDB, hSet, &K600TriggerSettings, size, MODE_READ, wirechamber_settings_callback, NULL)) != DB_SUCCESS){
		cm_msg(MERROR,"Frontend Init","unable to open data base record\n");
		cm_disconnect_experiment();
		return status;
  	}
	
//	cm_get_experiment_database(&hDB,NULL);
//	db_find_key(hDB,0,"/Equipment/WireChamber/Settings",&hkey);	
	//read Triggger settings
//  	size = sizeof(WIRECHAMBER_SETTINGS);
 // 	if ((status = db_get_record (hDB, hSet, &K600TriggerSettings, &size, 0)) != DB_SUCCESS){
//		cm_msg(MERROR,"ODB update failure"," failure to update online database");
///		return status;
  // 	}
	

  
	/* Base Address of Modules */
	V259_BASE	=	0xD0000;
	
	V792_BASE  	=	0xb0000;
//	V792_BASE  	=	0xe0000 
	
	V830_BASE[0]	=	0xe0000;
	V830_BASE[1]	=	0xf0000;
	
//	V1190_BASE[0]	=	0xd0000;
	V1190_BASE[0]	=	0x40000;
	V1190_BASE[1]	=	0x50000;
	V1190_BASE[2]	=	0x60000;
	V1190_BASE[3]	=	0x70000;
	V1190_BASE[4]	=	0x80000;
	V1190_BASE[5]	=	0x90000;
	V1190_BASE[6]	=	0xa0000;


	//assert(&K600TriggerSettings == NULL );
/*
	V1190_BASE[0]	=	K600TriggerSettings.v1190a_0.baseaddress;
	V1190_BASE[1]	=	K600TriggerSettings.v1190a_1.baseaddress;
	V1190_BASE[2]	=	K600TriggerSettings.v1190a_2.baseaddress;
	V1190_BASE[3]	=	K600TriggerSettings.v1190a_3.baseaddress;
	V1190_BASE[4]	=	K600TriggerSettings.v1190a_4.baseaddress;
	V1190_BASE[5]	=	K600TriggerSettings.v1190a_5.baseaddress;
	V1190_BASE[6]	=	K600TriggerSettings.v1190a_6.baseaddress;
*/	
	printf("[init] v1190 base address 0 is 0x%x \n",V1190_BASE[0] );

	printf("[init] v1190 base address 1 is %d \n",V1190_BASE[1] );
	printf("[init] v1190 base address 2 is %d \n",V1190_BASE[2] );
	printf("[init] v1190 base address 3 is %d \n",V1190_BASE[3] );
	printf("[init] v1190 base address 4 is %d \n",V1190_BASE[4] );
	printf("[init] v1190 base address 5 is %d \n",V1190_BASE[5] );

	printf("[init] v1190 base address 6 is %d \n",V1190_BASE[6] );

	N_TDC = K600TriggerSettings.chambers.global.max_tdc_channels;
/*
	for(tdcmod=0;tdcmod<V1190Count;tdcmod++){
		if(V1190_BASE[tdcmod]==0) {
			cm_msg(MERROR,"k600frontend init","EISH BANTWANA !! Zero base address for tdc module %d",tdcmod);
			if(tdcmod==6) V1190_BASE[tdcmod]=0xb0000;
			else V1190_BASE[tdcmod] = 0x40000+(tdcmod<<16);
			cm_msg(MERROR,"k600frontend init","EISH BANTWANA !! Setting module %d base address to default of 0x%08x",tdcmod,V1190_BASE[tdcmod]);
			printf("Setting module %d to have base address of : 0x%08x\n",tdcmod, V1190_BASE[tdcmod]);
		}
	}
	printf("[init] max tdc channels %d \n",N_TDC);
*/
	//create vme window
	//system("clear");
	printf(" # $ Frontend Hardware Initiliasation Of VME Window.... $ #\n");
/*	cm_msg(MINFO,"k600 frontend","Frontend Hardware Initiliasation Of VME Window");  */
	
	status = mvme_open(&k600vme,0);
	if (status!=1) { 
//		fprintf(stderr,"Exiting as status is %d\n",status);
		cm_msg(MERROR,"Frontend Init","Could Not open mvme device!");
		cm_disconnect_experiment();
		exit(1);
	}

	fprintf(stdout,"Frontend Compiled with Threads Enabled!\n");
	fprintf(stdout,"Frontend Initializing Thread[s].......\n");
	int result=0;
	result = init_thread();
	if( result != 1 ){ printf(" couldn't init thread \n"); exit(1); }
	fprintf(stdout,"Frontend Initializing Ring Buffer......\n");
	result = init_ringbuffer();
	if( result != 1 ){ printf(" couldn't init ring buffer  \n"); exit(1); }

	//int result;	
  	result = start_thread();
	if( result != SUCCESS ){
		printf("thread cant be started\n");
		exit(1);
	}


	//setup V792N
	printk("Frontend Hardware Initializing V792N.....\n");
	v792N_Setup(k600vme,V792_BASE,1);

	//setup the 1190 tdc modules
	//fprintf(stdout,"Frontend Hardware Initializing V1190 TDC Modules.....\n");
	int setupmode = 1;
	for(counter=0;counter< V1190Count;counter++) {
		printf(" ########## TDC MODULE [%d] Initialization ########## \n", counter );
		v1190A_Setup(k600vme,V1190_BASE[counter],setupmode);
	}

	//reset the 259 module
	//nothing to do to setup this module.
	//fprintf(stdout,"Frontend Hardware Initializing V259 Nothing To Do! \n");

	//reset the scalers

	printk("Frontend Hardware Initializing V830 Scalers......\n");
	int scalercount;
  	for(scalercount=0;scalercount<MaxScalers;scalercount++){
		v830_Setup(k600vme,V830_BASE[scalercount],0x3);
		v82X_Status(k600vme,V830_BASE[scalercount] ); 

		v82X_SoftClear(k600vme,V830_BASE[scalercount]);
	}


//	fprintf(stdout,"\n # $ Frontend Hardware Initialisation Completed! $ # \n ");

//	sleep(100);
//	system("clear");
	
#ifdef TIMING_TEMP

	if( (timerfd = open(DEVICE_TEMP, O_WRONLY)) < 0 ){
			printf("/dev/cctldt Open failed \n");
			exit(5);
	}

	printf("Timer openend on fd %d \n",timerfd );


#endif	


  // 	cm_msg(MINFO,"Frontend Init","finished testing access to online database for setup procedures.");
  	
//	system("clear");
	if(status!=1)return FE_ERR_HW;
   	else return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  frontend_exit
 *  Description :  When the frontend program is shut down. 
 *                 Can be used to releas any locked resources like memory, 
 *                 communications ports etc.
 *  Parameters  :  VOID
 *  Returns     :  SUCCESS, [ERROR]
 * =====================================================================================
 */
INT frontend_exit()
{
	int result;
	result = delete_ringbuffer(); 

    	mvme_close(k600vme);


   	return SUCCESS;
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  begin_of_run
 *  Description : When a new run is started. Clear scalers, open rungates, etc.
 *  Parameters  : INT run_number
 *  		  char* error 
 *  Returns     : SUCCESS,[ERROR]
 * =====================================================================================
 */
INT begin_of_run(INT run_number, char *error)
{
	int counter=0;
  	int i=0;
//  	int size;
//  	int status;
    	int csr;
/*
	//read Triggger settings
  	size = sizeof(WIRECHAMBER_SETTINGS);
  	if ((status = db_get_record (hDB, hSet, &K600TriggerSettings, &size, 0)) != DB_SUCCESS){
		cm_msg(MERROR,"ODB update failure"," failure to update online database");
		return status;
   	}
*/	
	//Setting Thresholds
	WORD thold[V792N_MAX_CHANNELS];
  	for (i=0;i<V792N_MAX_CHANNELS;i++) {
		printf("T%i:0x%4.4x ,", i, K600TriggerSettings.v792n_0.thresholds[i]);
    		thold[i] = K600TriggerSettings.v792n_0.thresholds[i];
  	}
	v792N_ThresholdWrite(k600vme, V792_BASE, thold );

	//Clearing v792N and checking stat reg
	v792N_DataClear(k600vme, V792_BASE);
  	csr = v792N_CSR1Read(k600vme, V792_BASE);
 	printf("Data Ready ADC0: 0x%08x\n", csr);

	// clear QDC
  	v792N_EvtCntReset(k600vme,V792_BASE);
  	v792N_SoftReset(k600vme,V792_BASE);

	// clear TDCS 
  	for(counter=0;counter<V1190Count;counter++) {
		v1190A_SoftClear(k600vme,V1190_BASE[counter]); 
  	}
	
	// clear scalers
	for(counter=0;counter<MaxScalers;counter++) {
		v82X_SoftClear(k600vme,V830_BASE[counter]);
	}
	memset(prevscalercount,0,18*sizeof(DWORD));


avgqdctime=0;
avgtdctime=0;
avgpattime=0;


	// clear pattern register.
	printf(" ###### $$$$$$$ SUCCESSFULLY PASSED BEGIN OF RUN  $$$$$$$ #######\n");

	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  end_of_run
 *  Description :  Called on a request to stop a run. 
 *  		   Can send end-of-run event and close run gates.
 *  Parameters  :  INT run_number
 *  		   char* error 
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT end_of_run(INT run_number, char *error)
{
	int result = stop_threads();
   	if( result != 1 ){ printf(" couldn't stop threads  \n"); exit(1); }
	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  pause_run
 *  Description :  When a run is paused. Should disable trigger events. 
 *  Parameters  :  INT run_number
 *  		   char* error 
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT pause_run(INT run_number, char *error)
{
	//stop scalers
	//clear tdc's
	int scalercount;
   	for(scalercount=0;scalercount<MaxScalers;scalercount++)
		v82X_SoftClear(k600vme,V830_BASE[scalercount]);
   
	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  resume_run
 *  Description :  When a run is resumed. Should enable trigger events.
 *  Parameters  :  INT run_number
 *  		   char* error
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT resume_run(INT run_number, char *error)
{
	//restarsaadq scalers
	//clear tdc's
	int scalercount;
   	for(scalercount=0;scalercount<MaxScalers;scalercount++)
		v82X_SoftClear(k600vme,V830_BASE[scalercount]);
   
	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  frontend_loop
 *  Description :   if frontend_call_loop is TRUE, this routine gets called, 
 *  		    when the frontend is idle or once between every event.
 *  Parameters  :  VOID
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT frontend_loop()
{
   return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  poll_event
 *  Description :  Polling routine for events. Returns TRUE if event is available. 
 *                 If test equals TRUE, don't return. 
 *                 The testflag is used to time the polling
 *  Parameters  :  INT source
 *  		   INT count
 *  		   BOOL test
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT poll_event(INT source, INT count, BOOL test)
{


	int nbytes;
	char *readpointer = NULL;
	char *wp = NULL;
	int result;
	int DataReady = 0;
	
	pthread_mutex_lock(&mutex);

	count_threaddata = count;

	printf("[poll_event] source %d \n",source );
	result = rb_get_rp(ring_buffer_handle,&readpointer,0);
	if( result != DB_SUCCESS){
		printf("[poll_event] couldn't get rb wp ! \n");
	//	exit(1);
		return 0;
	}else{
		printf("[poll_event] read pointer is 0x%x \n",&readpointer );
	}

	
//	memcpy( wp, &count, sizeof(count) );
//	printf("[poll_event] count %d, wp %d \n",count, *wp );
//	rb_increment_wp(ring_buffer_handle,sizeof(count));
//	rb_get_buffer_level(ring_buffer_handle,&nbytes);
//	printf("[poll_event] buffer level after writing %d \n",nbytes);


//	memcpy( wp, &test, sizeof(test) );
//	printf("[poll_event] test %d, wp %d \n",count, *wp );
//	rb_increment_wp(ring_buffer_handle,sizeof(test));
//	rb_get_buffer_level(ring_buffer_handle,&nbytes);
//	printf("[poll_event] buffer level after writing %d \n",nbytes);
	
	pthread_mutex_unlock(&mutex);


/*	
	pthread_mutex_lock(&mutex);

	rb_get_buffer_level(ring_buffer_handle,&nbytes);
	printf("[poll_event] buffer level before writing %d \n",nbytes);
	
	result = rb_get_rp(ring_buffer_handle,&readpointer,0);
	if( result != DB_SUCCESS){
		printf("[poll_event] couldn't get rb wp ! \n");
	//	exit(1);
		return 0;
	}else{
		printf("[poll_event] readpointer is 0x%x \n",&readpointer );
	}

	printf("[poll event] start pointer 0x%x \n",startringpointer );
	printf("[poll event] current read pointer 0x%x \n",readpointer );

	
	pthread_mutex_unlock(&mutex);
*/	

/*
	pthread_mutex_lock(&mutex);
	rb_get_rp(ring_buffer_handle,&rp,0);
        printf("[poll_event] rp[1] 0x%x \n ",(char*)rp );
	
	memcpy(&DataReady,rp,sizeof(DataReady));

//	rb_increment_wp(ring_buffer_handle,sizeof(pevent));
//    	rb_get_buffer_level(ring_buffer_handle,&nbytes );
//     	printf("[poll_event] ring buffer contains %d bytes , count = %d\n", nbytes,count );

	printf("[poll_event] DataReady %d \n",DataReady );
	if( DataReady )
		if(!test)
			return DataReady;
	
	pthread_mutex_unlock(&mutex);
*/
       	return 0;
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  interrupt_configure
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
	/* REMAINS TO BE IMPLEMENTED */
   	switch (cmd) {
		case CMD_INTERRUPT_ENABLE:
			break;
		case CMD_INTERRUPT_DISABLE:
			break;
   		case CMD_INTERRUPT_ATTACH:
      			break;
   		case CMD_INTERRUPT_DETACH:
      			break;
  	}

	return SUCCESS;
}

/* #####   Event Readout Routines FUNCTION DEFINITIONS  ################### */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_v792
 *  Description :  Event Readout for the v792 Module
 *  Parameters  :  int			base
 *  	           const char*		bank_name
 *  	           char*		pevent
 *  	           int			number_of_channels
 *  Returns     :  wcount,[ERROR]
 * =====================================================================================
 */
int read_v792(int base,const char*bank_name,char*pevent,int number_of_channels)
{
	int i;
  	int wcount = 0;
  	DWORD data[100];
  	unsigned int counter = 0;
	DWORD* pdata;
	unsigned int w;
	int chan,val;
	int valid[number_of_channels];
	int cmode;
	DWORD qdceventcount;
	  
  	/* Event counter */
  	v792N_EvtCntRead(k600vme, base, &counter);
  	//printf("792 counter = %d \n",counter);
	
      	if( pevent == NULL )
		printf("pevent NULL \n");	
#ifdef DEBUGC
	v792N_EvtCntRead(k600vme, V792_BASE, &qdceventcount);
	if(qdceventcount!=qdceventcounter+1)
		cm_msg(MINFO,"792 readout","event counter mismatch : %u!=%u+1",(unsigned int)qdceventcount,(unsigned int)qdceventcounter);
	qdceventcounter=qdceventcount;
#endif	
	/* Read Event */
	mvme_get_am(k600vme,&cmode);
  	mvme_set_am(k600vme, MVME_AM_A32_ND);
 	v792N_EventRead(k600vme, base, data, &wcount);
//  	v792N_DataRead(k600vme, base, data, &wcount);
  	mvme_set_am(k600vme,cmode );

  	/* create QDCS bank */
  	bk_create(pevent, bank_name, TID_DWORD, &pdata);

  	for (i=0; i<number_of_channels; i++){
		pdata[i] = 0;
    		valid[i]=0;
  	}

  	for (i=0; i<wcount; i++)
  	{
      		w = data[i];
      		//printf(" w  = 0x%.08x \n ",data[i] );
      		if (((w>>24)&0x7) != 0) continue;
      		chan = (w>>17)&0x1F;
      		val  = (w&0x0FFF);
      		//pdata[chan] = w;
      		*pdata++ = w;
		//	valid[chan]=1;
		//	if(((w>>12)&0x1)!=0){ pdata[chan]=0;valid[chan]=0;printf("overflow on chan : %d 0x%08x\n",chan,data[i]);} // overflow
		//	if(((w>>13)&0x1)!=0){ pdata[chan]=0; valid[chan]=0;printf("underflow on chan: %d 0x%08x\n",chan,data[i]);}// underflow
   	}	


#ifdef DEBUG 
	printf("counter %6d, words: %3d, header: 0x%08x, ADC0: 0x%08x, ADC0,1,2: %6d %6d %6d\n",
	       counter,wcount,data[0],pdata[0],pdata[0],pdata[1],pdata[2]);
	printf("counter %6d, words: %3d, header: 0x%08x,",
	       counter,wcount,data[0]);
	for(i=0;i<16;i++)printf("%d-%d:0x%04x ",i,valid[i],pdata[i]);
		 printf("\n");
#endif
		 
//  	for(i=0;i<number_of_channels;i++)pdata++;  //pdata += number_of_channels;//*sizeof(DWORD);

  	bk_close(pevent, pdata);
 	
	return wcount;
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_tdc
 *  Description :  Event Readout for TDC (v1190) Modules
 *  Parameters  :  char*		pevent
 *  Returns     :  [0],[ERROR]
 * =====================================================================================
 */
int read_tdc(char *pevent)
{
	int count;
	int wire=0;
	int geo;
	DWORD* pdata32;
	int edge,channel,time,code;
	int i;
	int xdata;
	int counts=200;
	int totalcounts=0;	// number of words
	int tdcmodulecounter=0;
	int wordcount[7];
	int datacount[7];

#ifdef DEBUGC	
	int cmode;
	DWORD diffcounter = 0;
	DWORD prevcounter = 0;
	DWORD prevevcounter = 0;
	DWORD eventdiffcounter = 0;
#endif
	/* create TOPA bank */
	bk_create(pevent, "TDC0", TID_DWORD, &pdata32);

	// Per TDC
	for(tdcmodulecounter=0;tdcmodulecounter<V1190Count;tdcmodulecounter++){
  		
//		memset(tdcdata,kDataSize,sizeof(DWORD));
//		memset(tdcdata,'\0',sizeof(kDataSize));
		
		geo=0;
	  	wordcount[tdcmodulecounter]=0;
		datacount[tdcmodulecounter]=0;
		
#ifdef DEBUGC
		prevcounter =  storedeventcounter[tdcmodulecounter];
		prevevcounter =  eventcounter[tdcmodulecounter];
	//	mvme_get_dmode(k600vme,&cmode);
  	//	mvme_set_dmode(k600vme, MVME_DMODE_D16);
  		storedeventcounter[tdcmodulecounter]  = v1190A_EvtStored(k600vme,V1190_BASE[tdcmodulecounter]);//mvme_read_value(k600vme, V1190_BASE[tdcmodulecounter]+0x1020);
		eventcounter[tdcmodulecounter]=v1190A_EvtCounter(k600vme,V1190_BASE[tdcmodulecounter]);
	//	mvme_set_dmode(k600vme,cmode );

//		diffcounter =  abs( storedeventcounter[tdcmodulecounter] - prevcounter );
		diffcounter =  abs( storedeventcounter[tdcmodulecounter] - prevcounter );
		eventdiffcounter=eventcounter[tdcmodulecounter]-prevevcounter;
		if( diffcounter > 1 ){
			cm_msg(MINFO,"Read_TDC","DataSkipped in mod %d diffcounter %d", tdcmodulecounter,diffcounter );
		}
		if(eventdiffcounter!=1){
			cm_msg(MINFO,"Read_TDC","DataSkipped eventcount in mod %d : %d != %d +1 ", tdcmodulecounter,eventcounter[tdcmodulecounter],prevevcounter);
		}
		
#endif

		count = v1190A_EventRead(k600vme, V1190_BASE[tdcmodulecounter], tdcdata, &counts);  //Single event read
		
//		count = v1190A_DataRead(k600vme, V1190_BASE[tdcmodulecounter], tdcdata, counts);   //DMA block level transfer (blt) Read
		
		//assert(data != NULL );
		
		//printf("[1] TDC count ---> <%d> , [2] TDC counts ---> <%d> \n",count,counts );

		if (counts > 0){ // we have data
//			counts=127; // TODO !!!!!!!! REMOVE THIS ITS DUE TO DataRead returning the number of words read which define to be 200 due to ensuring double wire firings.
#ifdef DEBUG
			if (counts > 127)
				printf("reading TDC got %d words\n",counts);
#endif
#ifdef DEBUG2
			printf("reading TDCmodule[0x%08x]  %d got %d words\n",V1190_BASE[tdcmodulecounter],tdcmodulecounter,counts);
#endif
			*pdata32++=tdcmodulecounter+(0x1f<<27);

			wordcount[tdcmodulecounter]=counts;
			for (i=0; i<counts; i++) {
				code = 0x1F&(tdcdata[i]>>27);

				if (tdcdata[i] == 0) continue;

				switch (code) {
					case 0: // valid measurent word 
						edge = 0x1&(tdcdata[i]>>26);
						channel = 0x7F&(tdcdata[i]>>19);
						time = 0x3FFFF&tdcdata[i];
						if(tdcmodulecounter==0){
							if(channel<2){
								wire=600+channel;
							}
//							switch(channel){
//								case 0 : printf("reftime : %d at wire %d\n",time,wire);break;
//								case 1 : printf("tof time : %d at wire %d\n",time,wire);break;
//							}
						}
//						xdata=time+((wire<<19)); //data can contain 19 bits of info, map the wire above that.
						*pdata32++ = tdcdata[i];
						datacount[tdcmodulecounter]++;
						#ifdef DEBUGDATA
 					  	printf("module : %d tdc %3d: 0x%08x, code 0x%02x, edge %d, chan %2d,time %6d\n", tdcmodulecounter, i, tdcdata[i], code, edge, channel, time);
						#endif
//						if ((channel==0) && (time0==0)) time0 = time;
//						if ((channel==1) && (time0!=0) && (time1==0) && (time>time0)) time1 = time;
//						if ((channel==2) && (time0!=0) && (time2==0) && (time>time0)) time2 = time;
        					break; // end of case 0
				}// switch statement
			} // for i
			totalcounts+=counts;
		} //count >0
#ifdef DEBUG2
		else		printf("count %d, we have no data\n",count);
#endif

		}//for tdcmodulecounter

  		bk_close(pevent, pdata32);

		//clear event buffer in module. Keeps the settings loaded into the modules.
		for(tdcmodulecounter=0;tdcmodulecounter<V1190Count;tdcmodulecounter++){
			v1190A_SoftClear(k600vme,V1190_BASE[tdcmodulecounter]);
		}


  		return totalcounts;
}

/* #####   FUNCTION DEFINITIONS - Misc. Functions -  ##################### */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  wirechamber_settings_callback
 *  Description :  Misc Func. Call Back for ODB
 *  Parameters  :  INT		hDB
 *  		   INT		hseq
 *  		   void*	info
 *  Returns     :  VOID 
 * =====================================================================================
 */
void wirechamber_settings_callback(INT hDB, INT hseq, void *info)
{
/*	
 	char set_str[128];
	int status=0;
	HNDLE hKey,key,hsubkey;
		
	printf("odb ... wirechamber settings touched with hseq : %d \n",hseq);
	cm_msg(MINFO,"WireChamberSettingsCallback","Settings for wirechamber have changed\n");
	sprintf(set_str, "/Equipment/WireChamber/Settings");
	status = db_find_key (hDB, 0, set_str, &hKey);
	db_copy_xml(hDB,hKey,xml_buffer,&xml_buffer_size);

	status= db_save_xml(hDB,hSet,"k600settings.xml");
	switch(status){
		case DB_SUCCESS : printf("successfully written xml to file : k600settings.xml\n");break;
		case DB_FILE_ERROR : printf("DB_FILE_ERROR on writing xml\n");break;
	}
*/
}

/* #####   FUNCTION DEFINITIONS  -  WireChamber and Scaler Functions   ##################### */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_wirechamber_event
 *  Description :   This involved reading you the v1990's, 
 *  		    v792, tdcs, qdc, hitpattern, respectively
 *  Parameters  :   char*	pevent
 *  		    INT		off
 *  Returns     :   INT (bk_size(pevent))
 * =====================================================================================
 */
INT read_wirechamber_event(char *pevent, INT off)
{
	//DWORD *pdata;
   	//DWORD EventCounter;
  	//int tdccount=0;
   	//int tdccounta=0;
  	//int qdccount=0;
  	//int tdcmodulecounter=0;
   	int nbanks;
   	int cmode;
   	char banklist[STRING_BANKLIST_MAX];
	int tdccounts=-1,qdccounts=-1,patcounts=-1;
	
	/* init bank structure */
	//bk_init32(pevent); for a bank of >32k events
	bk_init(pevent);

#ifdef DEBUG2 
	printf("Wire chamber event !!!\n");
#endif 

	nbanks=bk_list(pevent,banklist);
	// DRDY of v792 is only TRUE after conversion has taken place
	// we are there for now ready for readout, no waiting required.
	//Read QDC


	//  printf(" 0x%X \n",pevent);
	pthread_mutex_lock(&mutex);
	pevent_threaddata = pevent;
	pthread_mutex_unlock(&mutex);
	//  printf(" 0x%X \n",pevent_threaddata);
	//  thread_func(pevent_threaddata); 
/*
	qdccounts=read_v792(V792_BASE,"ADC0",pevent,V792N_MAX_CHANNELS);
*/

	//   nbanks=bk_list(pevent,banklist);
	//   printf("Event size is : %d\n",bk_size(pevent));
	//   printf("After qdc bank close #banks:%d List:%s\n",nbanks,banklist);
	
	//Read TDC's
/*
	tdccounts=read_tdc(pevent);
*/
	//   nbanks=bk_list(pevent,banklist);
	//   printf("Event size is ---->  %d\n",bk_size(pevent));
	//   printf("After tdc bank close #banks:%d List:%s\n",nbanks,banklist);
	//Read Pattern reg
	//printf("[1]Event size is : %d\n",bk_size(pevent));
	//printf("[2]After pattern register bank close #banks:%d List:%s\n",nbanks,banklist);

	//for safety sake clear and reset the qdc as this is fundamental to achieving a good trigger.

	//printf("Event size is : %d\n",bk_size(pevent));
	//printf("Wire chamber event finished returning bank size %d!!!\n",bk_size(pevent));

  	return bk_size(pevent);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_scalar_event
 *  Description :  Read out the scaler events from the V830.
 *  Parameters  :  char* 	pevent
 *  		   INT		off
 *  Returns     :  INT		(bk_size(pevent))
 * =====================================================================================
 */
INT read_scaler_event(char *pevent, INT off)
{
   DWORD *pdata;
   DWORD scalerchannel=0,scalercount=0,scalercounter=0;
   //unsigned int ChannelEnable=0;
   unsigned int ChannelBit=1;
   /* init bank structure */
   bk_init(pevent);

   /* create SCLR bank */
   bk_create(pevent, "SCLR", TID_DWORD, &pdata);
   for(scalercount=0;scalercount<MaxScalers;scalercount++){
	ChannelBit=1;	
//	ChannelEnable=v830_ReadChannelEnalbe(k600vme,V830_BASE[scalercount]);
   	for(scalerchannel=0;scalerchannel<MAXINPUTSCALERS;scalerchannel++){
	       //cm_msg(MINFO,"Scalerreadout","reading scaler %d, at 0x%08x at channel %d",scalercount,V830_BASE[scalercount],scalerchannel);	
	       //printf("reading scaler %d, at 0x%08x at channel %d",scalercount,V830_BASE[scalercount],scalerchannel);	
		scalercounter=v82X_ReadCounter(k600vme,V830_BASE[scalercount],scalerchannel);
		*pdata++=scalercounter-prevscalercount[scalercount][scalerchannel];
     	         prevscalercount[scalercount][scalerchannel]=scalercounter;
	}
   }
   bk_close(pevent, pdata);
#ifdef TIMING
   cm_msg(MINFO,"readouttiming","qdc :%d us, tdc : %d us, pat: %d us",avgqdctime,avgtdctime,avgpattime);
#endif
   return bk_size(pevent);
}




/* #####   FUNCTION DEFINITIONS  -  Thread Functions  ##################### */

INT init_ringbuffer()
{
	int result;
	// create circular buffer
	result = rb_create(2*max_event_size, max_event_size, &ring_buffer_handle);
//	rb_set_nonblocking();
	if( result != DB_SUCCESS  ) {
		printf(" Could not create ring buffer! result [%d] \n",result );
		exit(1);
	}

	result = rb_get_wp(ring_buffer_handle,  &startringpointer,0 );
	if( result != DB_SUCCESS ){
		printf(" Could not get read pointer from ringbuffer [%d] \n",result );
		exit(1);
	}

	printf(" startringpointer 0x%x \n",startringpointer );

 	int _nbytes;
 	rb_get_buffer_level(ring_buffer_handle,&_nbytes);
 	printf(" After ring buffer create, buffer contains %d bytes \n ",_nbytes);

	return SUCCESS;
}

INT delete_ringbuffer() 
{
	int result;
  	//result = ss_thread_kill(secondthread_id);
  	//pthread_exit(NULL);
  	
	result = rb_delete(ring_buffer_handle);
  	if( result != DB_SUCCESS ){
    		printf("RINGO cant be closed %d \n ",result );
		exit(1);
	}
   	return SUCCESS;
}

INT init_thread()
{
	int i;
	//create a single thread
	for( i = 0; i < MAX_CTHREADS; i++ ){
		threadstack[i].threadpid = 0;
		threadstack[i].active = 0;
		threadstack[i].threadnumber = i;	
		currentThread = i;	
	}
       parentthread_pid = getpid();
       return SUCCESS;
}

INT start_thread( ) 
{
	// start single thread!
	midas_thread_t thread_id;
// 	 pthread_create( &thread_id, NULL, thread_func, (void*)&pevent_threaddata  ) 

	pthread_attr_t pta;

	pthread_attr_init(&pta);
	pthread_attr_setdetachstate(&pta, PTHREAD_CREATE_JOINABLE);

	
//	thread_id = ss_thread_create( (void*)readout_threadfunc, &pevent_threaddata );	
	thread_id = ss_thread_create( (void*)readout_threadfunc, NULL );	
	if(thread_id == 0){
  		printf("error creating thread");
		abort(); 
 	}else{
		threadstack[currentThread].threadpid = getpid();
		threadstack[currentThread].active = 1;
		threadstack[currentThread].threadid = thread_id;
		printf("Thread created with id %lu and pid %1d \n",threadstack[currentThread].threadid,threadstack[currentThread].threadpid );
 	}

	return SUCCESS;
}


INT stop_threads() 
{
	int i;
	for( i = 0; i < MAX_CTHREADS; i++ ){
		midas_thread_t thread_id = threadstack[i].threadid;
//		pthread_join(thread_id,NULL);
		
		ss_thread_kill(thread_id);

		threadstack[i].threadpid  = 0;
		threadstack[i].active = 0;
		threadstack[i].threadid = 0;
		threadstack[i].threadnumber = -1;
	}	
	return SUCCESS;
}


void* sync_threadfunc(void* arg )
{


	return NULL;

}

// Thread worker function!
void* readout_threadfunc(void* arg) 
{
  int i,csr;	
  int status = 0;;
  char *pevent = NULL;
  void *p;
  int nbytes;
  pid_t pid;
  int DataReady = 0;
  int result;
  char *rp;
  BOOL test;
  int readoutloop = 0;

  char *wp;
  INT count;

//  printf(" [readout_threadfunc] arg [thread_func]  = 0x%x \n",(char*)arg ); 
//  pevent = (char*)arg;
//  printf(" [readout_threadfunc] pevent [thread_func]  = 0x%x \n", pevent ); 

//  printf(" stop_thread = %d \n ");

  
//  v792N_ThresholdWrite(k600vme, V792_BASE, (WORD *)&(K600TriggerSettings.v792n_0.thresholds));


//  printf("[readout_threadfunc] inside lock \n ");

  while(1){

	  if(setupdone) {
	  pthread_mutex_lock(&mutex); 
	  result = rb_get_rp(ring_buffer_handle,&rp,0);
	  if( result != DB_SUCCESS ){
	//	printf("[readout_threadfunc] could not get rp! \n");
		result = rb_get_wp(ring_buffer_handle,&wp,0);
		DataReady = 0;
		memcpy( wp, &DataReady,sizeof(DataReady));
		rb_increment_wp(ring_buffer_handle,sizeof(DataReady));
	//	runloop = 0;
	  }
	  else {
		
		  memcpy( &count, rp, sizeof(count) );
		  rb_increment_rp(ring_buffer_handle,sizeof(count));


		  for (i = 0; i < count; i++, DataReady++) {
			  DataReady = v792N_DataReady(k600vme,V792_BASE);
			  //printf("DataReady = %d \n ",DataReady );
			  if(DataReady){
//				  if(!test){
					result = rb_get_wp(ring_buffer_handle,&wp,0);
					memcpy( wp, &DataReady,sizeof(DataReady));
					rb_increment_wp(ring_buffer_handle,sizeof(DataReady));

//				  }

			  }
		  }


	   pthread_mutex_unlock(&mutex);
	}
	 /* 
		  //printf("[readout_threadfunc] count_threaddata %d \n",count_threaddata);
		  for (i = 0; i < count; i++, DataReady++) {
				DataReady = v792N_DataReady(k600vme,V792_BASE);
				if (DataReady){
				   rb_get_wp(ring_buffer_handle,&p,0);
				   printf(" wp[1] 0x%x , count_threaddata %d \n ",(char*)p,count_threaddata );
				   rb_increment_wp(ring_buffer_handle,sizeof(DataReady));
					
				}
				

		}
		*/
	 }//setupdone
  }//while
//   DataReady = DataReady_threaddata;	

//  printf("[readout_threadfunc] outside lock \n");

//  while(DataReady) {
   	
 // 	printf(" pid of this thread is %ld , and parent thread pid is %ld \n ",(long)pid,(long)parentthread_pid );
         
      
    //   status = rb_get_wp(ring_buffer_handle,&p,0);
       
    //   printf(" status: thread : %d \n ", status );
/*
 	if( stop_thread )
	 break;
       if( status == DB_TIMEOUT ){
	 printf("thread_func: Ring Buffer is full.... waiting for space !!! \n ");
	 ss_sleep(100);
	 continue;
       }
       if( status != DB_SUCCESS )
	 break;
 */
//       if(csr){ 
//           read_v792(V792_BASE,"ADC0",pevent,V792N_MAX_CHANNELS);
/*	   
	   if( sizeof(pevent) > (DWORD) max_event_size ){
	     printf(" event size too big \n ");
	     assert(FALSE);
	   }
*/
//	   rb_get_wp(ring_buffer_handle,&p,0);
//	   printf(" wp[1] 0x%x \n ",(char*)p );
//	   rb_increment_wp(ring_buffer_handle,sizeof(pevent));
	   
//	   rb_get_buffer_level(ring_buffer_handle,&nbytes );
  //	   printf(" ring buffer contains %d bytes \n", nbytes );
//	   if( nbytes >= 1000 )
//		   break;
//
//	   DataReady = v792N_DataReady(k600vme,V792_BASE);
//	   if(!DataReady)
//		   printf("Data Not Ready Anymore for the Zuma \n ");

  //     }
       
//  }
  // clear QDC
  //v792N_EvtCntReset(k600vme,V792_BASE);
  //v792N_SoftReset(k600vme,V792_BASE);

  return NULL;
}


