/*
 *  Simple test program -- simplified version of sample test hello.
 */

#include <bsp.h>

#include <stdlib.h>
#include <stdio.h>

/* #####   MIDAS HEADER FILE INCLUDES   ################################### */
#include "midas.h"
#include "mcstd.h"
#include "mvmestd.h"
#include "msystem.h"

#include "rtemsVME.h"
rtems_task Init(
  rtems_task_argument ignored
)
{

  int result = -1;
  int status;
  unsigned long addr;
  unsigned long V792_addr = 0x000e0000;
  unsigned long V1190_addr = 0xd0000;
  unsigned long V1495_addr = 0x40000000;
  unsigned long vmebase_addr = 0x0;
  unsigned long pcibase_addr = 0xC0000000;
  unsigned long pcibase_addr2 = 0xB0000000;
  unsigned long plocal = 0xffffffff;
  unsigned long offset = 0x1002;
  DWORD readvalue = 0xffffffff;
  int slot;
  int channel = 1;

  MVME_INTERFACE *myvme;
  
  int AddrType[] 
                = {
                VME_AM_SUP_SHORT_IO,
                VME_AM_STD_SUP_DATA,
                VME_AM_EXT_SUP_DATA, 
                VME_AM_CSR,
		VME_AM_STD_SUP_DATA 
            };
  
  
	
      BSP_vme_config();	
       
      BSP_VMEOutboundPortCfg(0, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16,0x00000000,0xc0000000,0x100000);
      BSP_VMEOutboundPortCfg(1, VME_AM_STD_SUP_DATA | VME_MODE_DBW16,0x00000000,0xb0000000,0x100000);
      BSP_VMEOutboundPortsShow(NULL);



/* MIDAS*/
	mvme_open(&myvme,0);
//	mvme_set_am(myvme,VME_AM_STD_SUP_DATA);
	mvme_set_am(myvme,MVME_AM_A24_ND);
	mvme_set_dmode(myvme,MVME_DMODE_D32);
//	mvme_set_dmode(myvme,VME_MODE_DBW16);
	addr = V792_addr + 0x1200;
 	readvalue = mvme_read_value(myvme,addr );
 	printk("\n [1]status register tdc: 0x%2x ", readvalue ); 
 	mvme_write_value(myvme, addr, 0x8);
	readvalue = mvme_read_value(myvme,addr );
 	printk("\n [2]status register tdc: 0x%2x ", readvalue ); 


/*
       BSP_VMEOutboundPortCfg(0, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16,0x00000000,0xc0000000,0x100000);
       BSP_VMEOutboundPortCfg(1, VME_AM_STD_SUP_DATA | VME_MODE_DBW16,0x00000000,0xb0000000,0x100000);
       BSP_VMEOutboundPortsShow(NULL);

       plocal = 0xc0000000 + V1190_addr + offset;
       printk("\n plocal = 0x%x \n",plocal);       
       printk("\n test 1: vme addr set to 0x0 in Port Config \n");
       printk("\n [1]status register tdc: 0x%2x [volatile uint16_t]", *(volatile uint16_t*)(0xc00d1002) ); 
       printk("\n [2]status register tdc: 0x%2x [volatile int]", *(volatile int*)(0xc00d1002) ); 
       printk("\n [2]status register tdc: 0x%2x [volatile char]", *(volatile char*)(0xc00d1002) ); 
       printk("\n [3]status register tdc: 0x%2x [volatile uint16_t]", *(volatile uint16_t*)(0xb00d1002) ); 
       printk("\n [4]status register tdc: 0x%2x [volatile int]", *(volatile int*)(0xb00d1002) ); 
       printk("\n [4]status register tdc: 0x%2x [volatile char]\n", *(volatile char*)(0xb00d1002) ); 
 
       BSP_VMEOutboundPortCfg(0, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16,0x000d0000,0xc0000000,0x100000);
       BSP_VMEOutboundPortCfg(1, VME_AM_STD_SUP_DATA | VME_MODE_DBW16,0x000d0000,0xb0000000,0x100000);
       BSP_VMEOutboundPortsShow(NULL);

       printk(" test 2: vme addr set to module addr in Port Config \n");
       printk("\n [1]status register tdc: 0x%2x [volatile uint16_t]", *(volatile uint16_t*)(0xc0001002) ); 
       printk("\n [2]status register tdc: 0x%2x [volatile int]", *(volatile int*)(0xc0001002) ); 
       printk("\n [2]status register tdc: 0x%2x [volatile char]", *(volatile char*)(0xc0001002) ); 
       printk("\n [3]status register tdc: 0x%2x [volatile uint16_t]", *(volatile uint16_t*)(0xb0001002) ); 
       printk("\n [4]status register tdc: 0x%2x [volatile int]", *(volatile int*)(0xb0001002) ); 
       printk("\n [4]status register tdc: 0x%2x [volatile char]\n", *(volatile char*)(0xb0001002) ); 

       BSP_VMEOutboundPortCfg(0, VME_AM_EXT_SUP_DATA | VME_MODE_DBW16,0x00000000,0xc0000000,0x100000);
       BSP_VMEOutboundPortCfg(1, VME_AM_STD_SUP_DATA | VME_MODE_DBW16,0x00000000,0xb0000000,0x100000);
       BSP_VMEOutboundPortsShow(NULL);
       result = BSP_vme2local_adrs( VME_AM_EXT_SUP_DATA | VME_MODE_DBW16  , V1190_addr , &plocal );
       printf("\n ADDRESS: %lx | READ: %2x \n ",(plocal + offset),*(volatile uint16_t*)(plocal + offset));



	
	int i = 0x0;
	for( i = 0x0; i < 0x20; i++ ) {
		offset = 0x1000 + i;
        	printf("\n i = [%d] || PORT: %d | ADDRESS: %lx | READ: %2x \n ",i,slot, (plocal + offset),*(int*)(plocal + offset));
		usleep(1000000);
	}
*/      
       
       
       
       
       
       
       
//printk("\n rtems mode %d , midas mode %d \n",VME_MODE_DBW16,MVME_DMODE_D16 );
// printk("\n addr = %x \n",addr );
//BSP_VMEOutboundPortsShow(NULL);

  exit( 0 );
}

/* configuration information */

/* NOTICE: the clock driver is explicitly disabled */
#define CONFIGURE_APPLICATION_DOES_NOT_NEED_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_MAXIMUM_TASKS            1

#define CONFIGURE_MAXIMUM_SEMAPHORES       5

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_USE_MINIIMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_INIT
#include <rtems/confdefs.h>

/* end of include file */
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#define CONFIGURE_MAXIMUM_TASKS 1

#define CONFIGURE_INIT

#include <rtems/confdefs.h>

/* end of file */
