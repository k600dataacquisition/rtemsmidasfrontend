/*
 * =====================================================================================
 *
 *       Filename:  system.h
 *
 *    Description:  
 *     Parameters:
 *        Version:  1.0
 *        Created:  10/05/2009 09:13:57 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator-Based Sciences
 *
 * =====================================================================================
 */


#include <rtems.h>

/* functions */

rtems_task Init(
  rtems_task_argument argument
);

rtems_task Application_task(
  rtems_task_argument argument
);

/* configuration information */

#include <bsp.h> /* for device driver prototypes */
#include <rtems/rtems_bsdnet.h>
#include <rtems/pci.h>

#include "networkconfig.h"


#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 20

#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

//#define CONFIGURE_EXTRA_TASK_STACKS 10*1024	/* delta value  */

/* lee */

#define CONFIGURE_MALLOC_STATISTICS

#define CONFIGURE_MINIMUM_STACK_SIZE 32768

#define CONFIGURE_MESSAGE_BUFFERS_FOR_QUEUE \
 (CONFIGURE_MESSAGE_BUFFERS_FOR_QUEUE( \
    500, sizeof(uint32_t) + \
  CONFIGURE_MESSAGE_BUFFERS_FOR_QUEUE( \
    500, sizeof(char) \
 )

#define CONFIGURE_MEMORY_OVERHEAD 122880	/* 120 MB */ 

/*
 * Keep track of the task id's created, use a large array.
 */

#define CONFIGURE_MAXIMUM_SEMAPHORES 20
#define CONFIGURE_MAXIMUM_TASKS      20 

#define CONFIGURE_MICROSECONDS_PER_TICK 1000  /* 1 millisecond */
#define CONFIGURE_TICKS_PER_TIMESLICE   10     /* 50 milliseconds */


#define CONFIGURE_INIT_TASK_STACK_SIZE 512*1024
#define CONFIGURE_INIT_TASK_PRIORITY   100 
#define CONFIGURE_INIT_TASK_INITIAL_MODES RTEMS_DEFAULT_MODES | RTEMS_PREEMPT | RTEMS_TIMESLICE
#define CONFIGURE_INIT_TASK_ATTRIBUTES (RTEMS_NO_FLOATING_POINT | RTEMS_LOCAL)

#define TASK_ALLOCATION_SIZE     (20)
#define CONFIGURE_EXTRA_TASK_STACKS (20 * RTEMS_MINIMUM_STACK_SIZE)
#define CONFIGURE_MAXIMUM_USER_EXTENSIONS (20)


/* 
 * #define CONFIGURE_INIT
 * rtems_task Init (rtems_task_argument argument);
*/

#include <rtems/confdefs.h>
