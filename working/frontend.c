/********************************************************************\

  Name:         frontend.c
  Created by:   Stefan Ritt

  Contents:     Experiment specific readout code (user part) of
                Midas frontend. This example simulates a "trigger
                event" and a "scaler event" which are filled with
                CAMAC or random data. The trigger event is filled
                with two banks (ADC0 and TDC0), the scaler event
                with one bank (SCLR).

  $Id: frontend.c 4089 2007-11-27 07:28:17Z ritt@PSI.CH $

\********************************************************************/

/* #####  C HEADER FILE INCLUDES   ######################################## */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include "rtemsnetwork.h"

/* #####   MIDAS HEADER FILE INCLUDES   ################################### */
#include "midas.h"
#include "mcstd.h"
//#include "/opt/vmecrates/concurrentvme/experim.h"
#include "experim.h"
#include "mvmestd.h"
#include "msystem.h"

#include <rtems/rtems/timer.h>

#include <rtems/confdefs.h>


/* #####   MODULES HEADER FILE INCLUDES  ################################## */


/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "Sample Frontend";
/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 3000;

/* maximum event size produced by this frontend */
INT max_event_size = 10000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 100 * 10000;

/* number of channels */
#define N_ADC  4
#define N_TDC  4
#define N_SCLR 4

/* CAMAC crate and slots */
#define CRATE      0
#define SLOT_IO   23
#define SLOT_ADC   1
#define SLOT_TDC   2
#define SLOT_SCLR  3

/*-- Function declarations -----------------------------------------*/

INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

INT read_trigger_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

void register_cnaf_callback(int debug);

extern HNDLE hDB;
HNDLE hSet;
WIRECHAMBER_SETTINGS K600TriggerSettings;

/* #################################################### */
extern INT mfe(char *ahost_name, char *aexp_name, BOOL adebug);


/*
 * RTEMS Startup Task
 */
rtems_task
Init (rtems_task_argument ignored)
{

  printk( "Initialize network\n" );
  rtems_bsdnet_initialize_network ();
  printk( "Network initialized\n" );
  rtems_bsdnet_show_inet_routes ();
 
  int numberofdriver = libbsdport_netdriver_dump(NULL);

 
  printf(" [Task Init] Mem assigned to CONFIGURE_EXECUTIVE_RAM_SIZE = %d \n ",CONFIGURE_EXECUTIVE_RAM_SIZE );	
  printf(" [Task Init] RTEMS_MINIMUM_STACK_SIZE = %d \n ",RTEMS_MINIMUM_STACK_SIZE );	

  printk( "Initiating Frontend\n" );
  mfe("xiafe","k600test",0 );
 // exit (0);
  rtems_task_delete(RTEMS_SELF);
}


/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {
   {"WireChamber",                /* equipment name */
    {1, 0,                   	  /* event ID, trigger mask */
     "K600SYSTEMS",               /* event buffer */
//     EQ_POLLED,                   /* equipment type */
     EQ_MULTITHREAD,
     LAM_SOURCE(0, 0xFFFFFF),     /* event source crate 0, all stations */
     "MIDAS",                     /* format */
     TRUE,                        /* enabled */
     RO_RUNNING |		  /* read only when running */
     RO_ODB,                      /* and update ODB */
     250,                         /* poll for 250ms */
     0,                           /* stop run after this event limit */
     0,                           /* number of sub events */
     0,                           /* don't log history */
     "concurrentvme", /* host on which front is running*/	
     "k600frontend", 		  /* frontend name*/
     "k600frontend.c",},  /* source file used for user FE  */	
    read_trigger_event,       /* readout routine*/
    },
#if 0
   {"Scaler",                     /* equipment name */
    {2, 0,                        /* event ID, trigger mask */
     "K600SYSTEMSCALER",          /* event buffer */
     EQ_PERIODIC | EQ_MANUAL_TRIG,/* equipment type */
     0,                           /* event source */
     "MIDAS",                     /* format */
     TRUE,                        /* enabled */
     RO_RUNNING | RO_TRANSITIONS |/* read when running and on transitions */
     RO_ODB,                      /* and update ODB */
     5000,                        /* read every 5 sec */
     0,                           /* stop run after this event limit */
     0,                           /* number of sub events */
     1,                           /* log history */
     "concurrentvme", /*host on which the front end is running*/
     "k600frontend",              /*front end name*/
     "k600frontend.c",}, /* source file used for user FE */
    read_scaler_event,            /* readout routine */
    },
#endif
   {""}
};

#ifdef __cplusplus
}
#endif

/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/*-- Frontend Init -------------------------------------------------*/

INT frontend_init()
{
   /* hardware initialization */

   cam_init();
   cam_crate_clear(CRATE);
   cam_crate_zinit(CRATE);

   /* enable LAM in IO unit */
   camc(CRATE, SLOT_IO, 0, 26);

   /* enable LAM in crate controller */
   cam_lam_enable(CRATE, SLOT_IO);

   /* reset external LAM Flip-Flop */
   camo(CRATE, SLOT_IO, 1, 16, 0xFF);
   camo(CRATE, SLOT_IO, 1, 16, 0);

   /* register CNAF functionality from cnaf_callback.c with debug output */
   register_cnaf_callback(1);

   /* print message and return FE_ERR_HW if frontend should not be started */

   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
   /* put here clear scalers etc. */

   return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/

INT frontend_loop()
{
   /* if frontend_call_loop is true, this routine gets called when
      the frontend is idle or once between every event */
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD lam;

   for (i = 0; i < count; i++) {
      cam_lam_read(LAM_SOURCE_CRATE(source), &lam);

      if (lam & LAM_SOURCE_STATION(source))
         if (!test)
            return lam;
   }

   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/

INT read_trigger_event(char *pevent, INT off)
{
   WORD *pdata, a;
   INT q, timeout;

   /* init bank structure */
   bk_init(pevent);

   /* create structured ADC0 bank */
   bk_create(pevent, "ADC0", TID_WORD, &pdata);

   /* wait for ADC conversion */
   for (timeout = 100; timeout > 0; timeout--) {
      camc_q(CRATE, SLOT_ADC, 0, 8, &q);
      if (q)
         break;
   }
   if (timeout == 0)
      ss_printf(0, 10, "No ADC gate!");

   /* use following code to read out real CAMAC ADC */
   /*
      for (a=0 ; a<N_ADC ; a++)
      cami(CRATE, SLOT_ADC, a, 0, pdata++);
    */

   /* Use following code to "simulate" data */
   for (a = 0; a < N_ADC; a++)
      *pdata++ = rand() % 1024;

   /* clear ADC */
   camc(CRATE, SLOT_ADC, 0, 9);

   bk_close(pevent, pdata);

   /* create variable length TDC bank */
   bk_create(pevent, "TDC0", TID_WORD, &pdata);

   /* use following code to read out real CAMAC TDC */
   /*
      for (a=0 ; a<N_TDC ; a++)
      cami(CRATE, SLOT_TDC, a, 0, pdata++);
    */

   /* Use following code to "simulate" data */
   for (a = 0; a < N_TDC; a++)
      *pdata++ = rand() % 1024;

   /* clear TDC */
   camc(CRATE, SLOT_TDC, 0, 9);

   bk_close(pevent, pdata);

   /* clear IO unit LAM */
   camc(CRATE, SLOT_IO, 0, 10);

   /* clear LAM in crate controller */
   cam_lam_clear(CRATE, SLOT_IO);

   /* reset external LAM Flip-Flop */
   camo(CRATE, SLOT_IO, 1, 16, 0xFF);
   camo(CRATE, SLOT_IO, 1, 16, 0);

   ss_sleep(10);

   return bk_size(pevent);
}

/*-- Scaler event --------------------------------------------------*/

INT read_scaler_event(char *pevent, INT off)
{
   DWORD *pdata, a;

   /* init bank structure */
   bk_init(pevent);

   /* create SCLR bank */
   bk_create(pevent, "SCLR", TID_DWORD, &pdata);

   /* read scaler bank */
   for (a = 0; a < N_SCLR; a++)
      cam24i(CRATE, SLOT_SCLR, a, 0, pdata++);

   bk_close(pevent, pdata);

   return bk_size(pevent);
}
