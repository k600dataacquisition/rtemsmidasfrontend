/*
 * =====================================================================================
 *
 *       Filename:  rtemsVME.h
 *
 *    Description:  RTEMS VME interface for mvmestd.h api.
 *
 *        Version:  1.0
 *        Created:  07/08/2009 11:21:06
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */
#ifndef RTEMSVME_H
#define RTEMSVME_H

#include <stdio.h>
#include <string.h>

#include "mvmestd.h"
#include <bsp/vme_am_defs.h>
#define _VME_UNIVERSE_DECLARE_SHOW_ROUTINES
#include <bsp/VME.h>
#include <bsp/VMEDMA.h>
#include <bsp/vmeUniverse.h>


#ifndef MIDAS_TYPE_DEFINED
typedef  unsigned long int   DWORD;
typedef  unsigned short int   WORD;
#endif



#ifndef  SUCCESS
#define  SUCCESS             (int) 1
#endif

#define ERROR 	(int) -1000
#define MVME_ERROR (int) -1000
#define  MAX_VME_SLOTS       (int) 32

#define  DEFAULT_SRC_ADD     0x50000000
#define  DEFAULT_NBYTES      0xFFFFFF     /* 16MB */ 
#define  DEFAULT_DMA_NBYTES      0x100000     /* 1MB */ 
/* #define  DEFAULT_NBYTES      4   */ /* 1MB */

/* Used for managing the map segments.
*/
typedef struct {
  int              am;
  mvme_size_t  nbytes;
  void           *ptr;
  mvme_addr_t     low;
  mvme_addr_t    high;
  int           valid;
} VME_TABLE;

typedef struct {
	int dma_handle;
	void *dma_ptr;
}DMA_INFO;

/* function declarations */
int rtems_mmap(MVME_INTERFACE *, mvme_addr_t, mvme_size_t);
mvme_addr_t rtems_mapcheck (MVME_INTERFACE *, mvme_addr_t, mvme_size_t);
long mvme_enableInterrupt( unsigned level ); 
long mvme_disableInterrupt( unsigned level );


#endif 

