/*
 * =====================================================================================
 *
 *       Filename:  rtemsVME.c
 *
 *    Description:  Accessing VME under RTEMS
 *
 *        Version:  1.0
 *        Created:  07/08/2009 11:20:38
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#include "rtemsVME.h"
#include <rtems.h>
#include <rtems/error.h>
#include <rtems/rtems/status.h>
/* #include <rtems/rtems/malloc.h> */
/* #include <rtems/malloc.h> */
/* #include <sys/malloc.h> */
#include <bsp/VME.h>
#include <bsp/VMEDMA.h>
#include <bsp/bspExt.h>

#include "mvmestd.h"

#include "rtemsVME.h"

#define DMACHANNEL 0

#define LOCAL2PCI(adrs) ((unsigned long)(adrs))

int PORT;

#ifndef VME_AM_CSR
#define VME_AM_CSR (0x2f)
#endif

mvme_addr_t rtems_mapcheck ( MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
  int j;
  VME_TABLE *table;
  mvme_addr_t addr;
  table = (VME_TABLE *) mvme->table;
 
 // printf(" mvme %p \n",mvme );
  //sleep(2);
  /* Extract window handle from table based on the VME address to read */
  for (j=0; table[j].valid; j++) {
    /* Check for the proper am */
    if (mvme->am != table[j].am)
      continue;
    /* Check the vme address range */
    if ((vme_addr >= table[j].low) && ((vme_addr+n_bytes) < table[j].high)) {
      /* valid range */
   //   printf("[%d] valid range \n",j);
      break;
    }
  } /*FOR*/


  /* Check if handle found or need to create new one */
  if (!table[j].valid) {
    /* Adjust vme_addr start point */
    addr = vme_addr & 0xFFF00000;
    /* Create a new window */
#ifdef DEBUGC  
    printk("\n -------========= creating new window !!!!! \n ");
#endif

    int mmap_value = rtems_mmap(mvme, addr, DEFAULT_NBYTES);
//    printk("\n mmap value %d \n",mmap_value );
    if ( mmap_value != MVME_SUCCESS) {
      perror("cannot create vme map window");
      return(MVME_ACCESS_ERROR);
    }

  }
  /* Get index in table in case new mapping has been requested */
  for (j=0; table[j].valid; j++) {
    /* Check for the proper am */
    if (mvme->am != table[j].am)
      continue;
    /* Check the vme address range */
    if ((vme_addr >= table[j].low) && ((vme_addr+n_bytes) < table[j].high)) {
      /* valid range */
      break;
    }
  }
  if (!table[j].valid) {
    perror("Map not found");
    return MVME_ACCESS_ERROR;
  }
  addr = (mvme_addr_t) (table[j].ptr) + (mvme_addr_t) (vme_addr - table[j].low);
  return addr;
}



int rtems_mmap(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
  int status;
  int j;
  VME_TABLE *table;
 
  

  table = (VME_TABLE *) mvme->table;
 

  /* Find new slot */
  j=0;
  while (table[j].valid)  j++;
  
  if (j < MAX_VME_SLOTS) {
    /* Create a new window */
    table[j].low    = vme_addr;
    table[j].am     = ((mvme->am == 0) ? MVME_AM_DEFAULT : mvme->am);
    table[j].nbytes = n_bytes;
#ifdef DEBUGC
    printk("\n default window --> vme_addr = 0X%x, nbytes = %d \n",vme_addr, n_bytes );
    printk("\n table[%d].low = 0x%x : table[%d].am = %X : table[%d].nbytes = %d \n",j,table[j].low,j,table[j].am,j,table[j].nbytes );
#endif
 /*   status = sysBusToLocalAdrs(VME_AM_STD_USR_DATA, (char *)table[j].low, (char **)&(table[j].ptr)); */

 //   BSP_VMEOutboundPortsShow(NULL);

 /*   status = BSP_vme2local_adrs(VME_AM_EXT_SUP_DATA, (volatile char *)table[j].low, (volatile char **)&(table[j].ptr)); 
  */ 
    
//     printf(" mvme %p \n",mvme );
 //   printk("\n BSP_vme2local : vme address = 0x%x , plocal = 0x%x \n", (char *)table[j].low, (char *)(table[j].ptr) );

    status = BSP_vme2local_adrs(VME_AM_EXT_SUP_DATA, (unsigned long)table[j].low, (unsigned long *)&(table[j].ptr)); 

   //  printk("\n BSP_vme2local : vme address = 0x%x , plocal = 0x%x \n", (char *)table[j].low, (char *)(table[j].ptr) );     
#ifdef DEBUGC
     printk("\n status = %d \n");
#endif
   if (status == -1   ) {
      printk("\n doing a memset------------ \n");
      memset(&table[j], 0, sizeof(VME_TABLE));
      return(MVME_ACCESS_ERROR);
    }
    table[j].valid = 1;
    table[j].high  = (table[j].low + table[j].nbytes);
  }
  else {
    /* No more slot available */
    return(MVME_ACCESS_ERROR);
  }
  
  return(MVME_SUCCESS);
}


/*-----------------------------------------------------------------------------
 *   MIDAS API
 *-----------------------------------------------------------------------------*/

int mvme_open(MVME_INTERFACE **mvme, int index)
{

   int rval = -1;

   BSP_vme_config();	
       

 /* Allocate MVME_INTERFACE */
  *mvme = (MVME_INTERFACE *) malloc(sizeof(MVME_INTERFACE));

	  
  memset((char *) *mvme, 0, sizeof(MVME_INTERFACE));
  
  /* Allocte VME_TABLE */
  (*mvme)->table = (char *) malloc(MAX_VME_SLOTS * sizeof(VME_TABLE));
  
  /* initialize the table */
  memset((char *) (*mvme)->table, 0, MAX_VME_SLOTS * sizeof(VME_TABLE));
  
  /* Set default parameters */
  (*mvme)->am = MVME_AM_DEFAULT;

   //BSP_VMEDmaStatus(DMACHANNEL);

   uint32_t rtemsVmeDmaBusMode = BSP_VMEDMA_OPT_DEFAULT;
	/* DMA direct transfer Setup */	
	uint32_t mode = ( MVME_AM_A32_NB | MVME_DMODE_D64  );
	rval = BSP_VMEDmaSetup( DMACHANNEL, rtemsVmeDmaBusMode, mode, 0 );
	if( rval != 0 ){
		printf("[mvme_read_dma]: dma status %d \n",BSP_VMEDmaStatus(DMACHANNEL));
		return(MVME_ERROR);
	}
	
  /* create a default master window 
  if ((rtems_mmap(*mvme, DEFAULT_SRC_ADD, DEFAULT_NBYTES)) != MVME_SUCCESS) {
    perror("cannot create vme map window");
    return(MVME_ACCESS_ERROR);
  }
  */
    
  return(MVME_SUCCESS);
}




/********************************************************************/
/**
Close and release ALL the opened VME channel.
@param *mvme     VME structure.
@return MVME_SUCCESS, ERROR
*/
int mvme_close(MVME_INTERFACE *mvme)
{
  /* debug */
  printf("mvme_close\n");
  
  /* Free table pointer */
  free (mvme->table);
  
  mvme->table = NULL;
  
  /* Free mvme block */
  free (mvme);
  
  return(MVME_SUCCESS);
}

/********************************************************************/
/**
Read single data from VME bus.
@param *mvme VME structure
@param vme_addr source address (VME location).
@return value return VME data
*/
DWORD mvme_read_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr)
{
  mvme_addr_t addr;
  DWORD dst = 0xffffffff;
  DWORD plocal = 0xffffffff;
  
   addr = rtems_mapcheck(mvme, vme_addr, 4); 
  
//   printk("\n [2] READING Adrr ===== 0x%x :: READ[2] %2x\n",addr, *(volatile uint16_t*)addr ); 
  
   /* Perform read */
  if (mvme->dmode == MVME_DMODE_D8){
    dst  = *((volatile uint8_t *)addr);
  }
  else if (mvme->dmode == MVME_DMODE_D16){
    dst  = *((volatile uint16_t *)addr);
  }
  else if (mvme->dmode == MVME_DMODE_D32){
    dst = *((volatile DWORD *)addr);
  }
  
  
  return dst;
}



int mvme_read_dma(MVME_INTERFACE *mvme, void *dst, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
	int rval;
	int status;
    

	/* DMA start the transfer */
	rval = BSP_VMEDmaStart( DMACHANNEL, dst, vme_addr, n_bytes );
	if( rval != 0 ){
		printf("[mvme_read_dma]: error BSP_VMEDmaStart rval = %d , vme_addr %8x, dst %p, n_bytes %d \n",rval,vme_addr,dst,n_bytes );
		printf("[mvme_read_dma]: dma status %d \n",BSP_VMEDmaStatus(DMACHANNEL));
		return(MVME_ERROR);
	}
	
	//int count= 0;
	//printf("\n DMA Reading ");
	
	while( BSP_VMEDmaStatus(DMACHANNEL) != BSP_VMEDMA_STATUS_OK ){
#ifdef DEBUGC
		printf(".");
#endif
		//count++;
	}
	
	//printf("\n DMA done: status %d \n ", BSP_VMEDmaStatus(DMACHANNEL) );
	
	return(MVME_SUCCESS);
}


/*  DMA transfer ( BLT32/BLT64/CBLT32/CBLT64 )
 *
 *
 *
 *
 *
 */
int mvme_read(MVME_INTERFACE *mvme, void *dst, mvme_addr_t vme_addr, mvme_size_t n_bytes)
{
  int         status;
  DWORD dstv = 0xffffffff;

  /* Perform read */
  /*--------------- DMA --------------*/
  if ((mvme->blt_mode == MVME_BLT_BLT32) ||
      (mvme->blt_mode == MVME_BLT_MBLT64) ||
      (n_bytes > 127)) {
  
	  //  printf("[mvme_read] ############## DMA TRANSFER ############# \n");
	    int am = mvme->am;
	     


	    DMA_INFO *info = ((DMA_INFO*)mvme->info);

	    if (n_bytes >= DEFAULT_DMA_NBYTES)
	      {
		printf("mvme_read: Attempt to DMA %d bytes more than DEFAULT_DMA_NBYTES=%d\n", (int)n_bytes, DEFAULT_DMA_NBYTES);
		return(ERROR);
	      }

	    if (mvme->blt_mode == MVME_BLT_BLT32)
	      am = MVME_AM_A32_NB;
	    else if (mvme->blt_mode == MVME_BLT_MBLT64)
	      am = MVME_AM_A32_NMBLT;


	   status =  mvme_read_dma(mvme, dst, vme_addr, n_bytes);

	    if (status != MVME_SUCCESS) {
	      printf("[mvme_read] dma Error status %d \n", status );
	      return(ERROR);
	    }

	    /* copy data read to info->vme_ptr */
	    //memcpy(dst,info->dma_ptr,n_bytes);

  } else {
   	  
	  mvme_addr_t addr;
    	  addr = rtems_mapcheck(mvme, vme_addr, 4); 
  
//printk("\n [2] READING Adrr ===== 0x%x :: READ[2] %2x\n",addr, *(volatile uint16_t*)addr ); 
 
	   /* Perform read */
	  if (mvme->dmode == MVME_DMODE_D8){
	    dstv  = *((volatile uint8_t *)addr);
	  }
	  else if (mvme->dmode == MVME_DMODE_D16){
	    dstv  = *((volatile uint16_t *)addr);
	  }
	  else if (mvme->dmode == MVME_DMODE_D32){
	    dstv = *((volatile DWORD *)addr);
	  }
	 
  }/* if-else */

  dst = &dstv;
  
  return(MVME_SUCCESS);
}



/********************************************************************/
/**
Write single data to VME bus.
@param *mvme VME structure
@param vme_addr destination address (VME location).
@param value  data to write to the VME address.
@return MVME_SUCCESS
*/
int mvme_write_value(MVME_INTERFACE *mvme, mvme_addr_t vme_addr, DWORD value)
{
  mvme_addr_t addr;
  DWORD plocal = 0xffffffff;
   
  addr = rtems_mapcheck(mvme, vme_addr, 4);
 
  /*
  BSP_vme2local_adrs( VME_AM_EXT_SUP_DATA | VME_MODE_DBW16  , vme_addr , &plocal );
  addr = plocal;
  */


  /* Perform write */
  if (mvme->dmode == MVME_DMODE_D8){
    *((char *)addr)  = (char) (value &  0xFF);
  
  } else if (mvme->dmode == MVME_DMODE_D16){
    //*((WORD *)addr)  = (WORD) (value &  0xFFFF);
    *((volatile uint16_t *)addr)  = (volatile uint16_t) (value &  0xFFFF);
  
  } else if (mvme->dmode == MVME_DMODE_D32) {
    *((volatile DWORD *)addr)  = value;
  }
  
  return MVME_SUCCESS;
}



int mvme_set_blt(MVME_INTERFACE *mvme, int mode )
{
	mvme->blt_mode = mode;
	return MVME_SUCCESS;
}

int mvme_get_blt(MVME_INTERFACE *mvme, int *mode )
{
	*mode = mvme->blt_mode;
	return MVME_SUCCESS;
}

/********************************************************************/
int mvme_set_am(MVME_INTERFACE *mvme, int am)
{
  mvme->am = am;
  return MVME_SUCCESS;
}

/********************************************************************/
int EXPRT mvme_get_am(MVME_INTERFACE *mvme, int *am)
{
  *am = mvme->am;
  return MVME_SUCCESS;
}

/********************************************************************/
int mvme_set_dmode(MVME_INTERFACE *mvme, int dmode)
{
  mvme->dmode = dmode;
  return MVME_SUCCESS;
}

/********************************************************************/
int mvme_get_dmode(MVME_INTERFACE *mvme, int *dmode)
{
  *dmode = mvme->dmode;
  return MVME_SUCCESS;
}

long mvme_enableInterrupt( unsigned level )
{
	return BSP_enableVME_int_lvl(level);
}


long mvme_disableInterrupt( unsigned level )
{
	return BSP_disableVME_int_lvl(level);
}


/*------------------------------------------------------------------*/

