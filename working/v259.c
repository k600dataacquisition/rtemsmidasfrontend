#include "v259.h"
#include <stdio.h>
#include <assert.h>

/**
 * Read and display the curent status of the module.
 * @param *mvme VME structure
 * @param  base Module base address
 * @return MVME_SUCCESS
 */
int  v259_Status(MVME_INTERFACE *mvme, DWORD base) {
	union {
		struct {
			unsigned type:10;
			unsigned N:6;
		};
		unsigned short raw;
	} manufact;
	union {
		struct {
			unsigned serial:12;
			unsigned version:4;
		};
		unsigned short raw;
	} version;
	int   cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	//-------------------------------------------------
	printf("\n--- ID:\n");
	WORD v = mvme_read_value(mvme, base+V259_ID_RO);
	printf("  ID                       : 0x%04x\n", v);
	version.raw=mvme_read_value(mvme, base+V259_VERSION_RO);
	printf("  Serial                   : 0x%04x\n", version.serial);
	printf("  Version                  : 0x%04x\n", version.version );
	manufact.raw=mvme_read_value(mvme, base+V259_MANUFACT_RO);
	printf("  Manufacturer N           : 0x%04x\n", manufact.N);
	printf("  Type                     : 0x%04x\n", manufact.type );
	
	//-------------------------------------------------
	
	mvme_set_dmode(mvme, cmode);
	return 0;
}
		
/*****************************************************************/
void v259_SoftReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V259_MODULE_RESET_WO, 0);
  mvme_set_dmode(mvme, cmode);
}



/**
 * Read Pattern and Reset
 * @param *mvme VME structure
 * @param  base Module base address
 * @param n counter number (0-31)
 */
WORD v259_ReadPatternReset(MVME_INTERFACE *mvme, DWORD base)
{
	int cmode;
	WORD v;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	
	v=mvme_read_value(mvme, base+V259_PATTERN_RST_RO);
	
	mvme_set_dmode(mvme, cmode);
	return v;
}


/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
