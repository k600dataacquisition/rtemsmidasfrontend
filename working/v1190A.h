/*********************************************************************

  Name:         v1190A.h
  Created by:   Pierre-Andre Amaudruz
  Modified by: 	SHTM 
  Contents:     V1190A 64ch. TDC
                
  $Log: v1190B.h,v $
*********************************************************************/

#include "mvmestd.h"

#ifndef V1190A_INCLUDE_H
#define V1190A_INCLUDE_H


/* V1190 Base address */
#define  V1190A_MAX_CHANNELS      (DWORD) 128
#define  V1190A_REG_BASE          (DWORD) (0x1000)
#define  V1190A_CR_RW             (DWORD) (0x1000)
#define  V1190A_SR_RO             (DWORD) (0x1002)
#define  V1190A_DATA_READY        (DWORD) (0x0001)
#define  V1190A_ALMOST_FULL       (DWORD) (0x0002)
#define  V1190A_FULL              (DWORD) (0x0004)
#define  V1190A_TRIGGER_MATCH     (DWORD) (0x0008)
#define  V1190A_HEADER_ENABLE     (DWORD) (0x0010)
#define  V1190A_GEO_REG_RW        (DWORD) (0x100E)
#define  V1190A_MODULE_RESET_WO   (DWORD) (0x1014)
#define  V1190A_SOFT_CLEAR_WO     (DWORD) (0x1016)
#define  V1190A_SOFT_EVT_RESET_WO (DWORD) (0x1018)
#define  V1190A_SOFT_TRIGGER_WO   (DWORD) (0x101A)
#define  V1190A_EVT_CNT_RO        (DWORD) (0x101C)
#define  V1190A_EVT_STORED_RO     (DWORD) (0x1020)
#define  V1190A_FIRM_REV_RO       (DWORD) (0x1026)
#define  V1190A_MICRO_HAND_RO     (DWORD) (0x1030)
#define  V1190A_MICRO_RW          (DWORD) (0x102E)
/* Micro code IDs */
#define  V1190A_WINDOW_WIDTH_WO   (WORD) (0x1000)
#define  V1190A_WINDOW_OFFSET_WO  (WORD) (0x1100)
#define  V1190A_MICRO_WR_OK       (WORD) (0x0001)
#define  V1190A_MICRO_RD_OK       (WORD) (0x0002)
#define  V1190A_MICRO_TDCID       (WORD) (0x6000)
#define  V1190A_EDGE_DETECTION_WO (WORD) (0x2200) 
#define  V1190A_LE_RESOLUTION_WO  (WORD) (0x2400)
#define  V1190A_LEW_RESOLUTION_WO (WORD) (0x2500)
#define  V1190A_RESOLUTION_RO     (WORD) (0x2600)
#define  V1190A_TRIGGER_MATCH_WO  (WORD) (0x0000)
#define  V1190A_CONTINUOUS_WO     (WORD) (0x0100)
#define  V1190A_ACQ_MODE_RO       (WORD) (0x0200)
 
#define  LE_RESOLUTION_100       (WORD) (0x10)
#define  LE_RESOLUTION_200       (WORD) (0x01)
#define  LE_RESOLUTION_800       (WORD) (0x00)

int  udelay(int usec);
int  udelay2(int usec);
int  v1190A_EventRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry);
int  v1190A_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry);
void v1190A_SoftReset(MVME_INTERFACE *mvme, DWORD base);
void v1190A_SoftClear(MVME_INTERFACE *mvme, DWORD base);
void v1190A_SoftTrigger(MVME_INTERFACE *mvme, DWORD base);
void v1190A_DataReset(MVME_INTERFACE *mvme, DWORD base);
int  v1190A_DataReady(MVME_INTERFACE *mvme, DWORD base);
int  v1190A_EvtCounter(MVME_INTERFACE *mvme, DWORD base);
int  v1190A_EvtStored(MVME_INTERFACE *mvme, DWORD base);
int  v1190A_MicroCheck(MVME_INTERFACE *mvme, const DWORD base, int what);
int  v1190A_MicroWrite(MVME_INTERFACE *mvme, DWORD base, WORD data);
int  v1190A_MicroRead(MVME_INTERFACE *mvme, const DWORD base);
int  v1190A_MicroFlush(MVME_INTERFACE *mvme, const DWORD base);
void v1190A_TdcIdList(MVME_INTERFACE *mvme, DWORD base);
int  v1190A_ResolutionRead(MVME_INTERFACE *mvme, DWORD base);
void v1190A_LEResolutionSet(MVME_INTERFACE *mvme, DWORD base, WORD le);
void v1190A_LEWResolutionSet(MVME_INTERFACE *mvme, DWORD base, WORD le, WORD width);
void v1190A_TriggerMatchingSet(MVME_INTERFACE *mvme, DWORD base);
void v1190A_AcqModeRead(MVME_INTERFACE *mvme, DWORD base);
void v1190A_ContinuousSet(MVME_INTERFACE *mvme, DWORD base);
void v1190A_WidthSet(MVME_INTERFACE *mvme, DWORD base, WORD width);
void v1190A_OffsetSet(MVME_INTERFACE *mvme, DWORD base, WORD offset);
int  v1190A_GeoWrite(MVME_INTERFACE *mvme, DWORD base, int geo);
int  v1190A_Setup(MVME_INTERFACE *mvme, DWORD base, int mode);
int v1190A_TriggerConfig(MVME_INTERFACE *mvme, DWORD base, short *trigger_config);
int  v1190A_Status(MVME_INTERFACE *mvme, DWORD base);
void v1190A_SetEdgeDetection(MVME_INTERFACE *mvme, DWORD base, int eLeading, int eTrailing);



  enum v1190A_DataType {
    v1190A_GlobalHeader=0x08,
    v1190A_GlobalTrailer=0x10,
    v1190A_TDCHeader=0x01,
    v1190A_TDCTrailer=0x03,
    v1190A_TDCError=0x04,
    v1190A_Measurement=0x00,
    v1190A_ExtendedTimeTag=0x11,
    v1190A_Filler=0x18
  };

  typedef union {
    DWORD raw;
    struct GlobalHeader {
      unsigned geo:5; // bit0 here
      unsigned event_count:22;
      unsigned type:5;
    } gheader ;
    struct TDCHeader {
      unsigned bunchid:12; // bit0 here
      unsigned eventid:12;
      unsigned tdc:2;
      unsigned pad:1;
      unsigned type:5;
    } tdcheader;
    struct Data {
      unsigned time:19; // bit0 here
      unsigned channel:7;
      unsigned trailing:1;
      unsigned type:5;
    } measurement;
    struct TDCTrailer{
      unsigned wordcount:12; // bit0 here
      unsigned eventid:12;
      unsigned tdc:2;
      unsigned _pad_1:1;
      unsigned type:5;
    } tdctrailer;
    struct TriggerTimeTag{
      unsigned timetage:27; // bit 0 here
      unsigned type:5;
    } triggertimetag;
   struct GlobalTrailer{
      unsigned geo:5; // bit0 here
      unsigned word_count:15;
      unsigned _pad_1:3;
      unsigned status:3;
      unsigned type:5;
   } globaltrailer;
   struct TDCError{
      unsigned errorflags:15;
      unsigned _pad_1:9;
      unsigned tdc:2;
      unsigned _pad_2:1;
      unsigned type:5;
   } tdcerror;
  } v1190A_Data;    

#endif // V1190A_INCLUDE_H
