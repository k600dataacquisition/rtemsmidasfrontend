/*
 * =====================================================================================
 *
 *       Filename:  vmetest.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/07/2009 08:36:53
 *       Revision:  none
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog:
 * =====================================================================================
 */
#ifndef VMETEST_H
#define VMETEST_H

#define _VME_UNIVERSE_DECLARE_SHOW_ROUTINES
#include <bsp/VME.h>
#include <bsp/VMEDMA.h>
#include <bsp/vmeUniverse.h>


/*-----------------------------------------------------------------------------
 *  VME STUFF
 *-----------------------------------------------------------------------------*/
typedef unsigned long DWORD_t;
typedef unsigned int WORD_t;
typedef unsigned long vmeaddr_t;
typedef unsigned long address_mod_t;
typedef unsigned long address_space_t;



/*-----------------------------------------------------------------------------
 *   RTEMS CONFIGURATION SETUP
 *-----------------------------------------------------------------------------*/
/* configuration information */
#define CONFIGURE_APPLICATION_EXTRA_DRIVERS  TTY1_DRIVER_TABLE_ENTRY
#define RTEMS_BSP_HAS_IDE_DRIVER

#ifdef RTEMS_BSP_HAS_IDE_DRIVER
#include <libchip/ata.h> /* for ata driver prototype */
#include <libchip/ide_ctrl.h> /* for general ide driver prototype */
#endif
/* NOTICE: the clock driver is explicitly disabled */
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER

#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER


#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_MAXIMUM_SEMAPHORES       20

#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

#ifdef RTEMS_BSP_HAS_IDE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_IDE_DRIVER
#endif

#define CONFIGURE_APPLICATION_NEEDS_LIBBLOCK


#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES    20

#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 6

#define STACK_CHECKER_ON



#endif


