/*
 * RTEMS configuration/initialization
 * 
 * This program may be distributed and used for any purpose.
 * I ask only that you:
 *    1. Leave this author information intact.
 *    2. Document any changes you make.
 *
 * W. Eric Norum
 * Saskatchewan Accelerator Laboratory
 * University of Saskatchewan
 * Saskatoon, Saskatchewan, CANADA
 * eric@skatter.usask.ca
 *
 *  $Id: init.c,v 1.14 2009/05/12 19:59:47 joel Exp $
 */
#include <stdio.h>
#include <stdlib.h>
#include <bsp.h>
#include <rtems.h>
#include <rtems/rtems_bsdnet.h>
#include "networkconfig.h"

//extern int mfe(char *ahost_name, char *aexp_name, BOOL adebug);
/*
 * RTEMS Startup Task
 */
rtems_task
Init (rtems_task_argument ignored)
{
  rtems_name        task_name;
  rtems_id          tid;
  rtems_status_code status;

  printk( "Initialize network\n" );
  rtems_bsdnet_initialize_network ();
  printk( "Network initialized\n" );
  rtems_bsdnet_show_inet_routes ();
 
  

//  int numberofdriver = libbsdport_netdriver_dump(NULL);
//
// 
//  printf(" [Task Init] Mem assigned to CONFIGURE_EXECUTIVE_RAM_SIZE = %d \n ",CONFIGURE_EXECUTIVE_RAM_SIZE );	
//  printf(" [Task Init] RTEMS_MINIMUM_STACK_SIZE = %d \n ",RTEMS_MINIMUM_STACK_SIZE );	
//
 
//
//  printf( "\n\n*** SAMPLE SINGLE PROCESSOR APPLICATION ***\n" );
//  printf( "Creating and starting an application task\n" );
//
//  task_name = rtems_build_name( 'T', 'A', '1', ' ' );
//
//  status = rtems_task_create( task_name, 100 , RTEMS_MINIMUM_STACK_SIZE ,  RTEMS_DEFAULT_MODES | RTEMS_PREEMPT | RTEMS_TIMESLICE , RTEMS_DEFAULT_ATTRIBUTES, &tid );
//
  
//  status = rtems_task_start( tid, Application_task, 0 );


//  stopThread = 1;

//  int i = 0;
//  while( i < 100){
//	  printf(" HELLO WORLD \n");
//	  i++;
 // }
    /* Start the Frontend */
  mfe("xiafe","k600test",0 );
  stopThread = 0;
 
 
 
  rtems_task_delete(RTEMS_SELF);
}


#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_RTEMS_INIT_TASKS_TABLE
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS 20
#define CONFIGURE_USE_IMFS_AS_BASE_FILESYSTEM

#define CONFIGURE_EXECUTIVE_RAM_SIZE (512*1024)
#define CONFIGURE_MAXIMUM_SEMAPHORES 20
#define CONFIGURE_MAXIMUM_TASKS      20

#define CONFIGURE_MICROSECONDS_PER_TICK 10000

#define CONFIGURE_INIT_TASK_STACK_SIZE (10*1024)
#define CONFIGURE_INIT_TASK_PRIORITY   120
#define CONFIGURE_INIT_TASK_INITIAL_MODES (RTEMS_PREEMPT | \
                                           RTEMS_NO_TIMESLICE | \
                                           RTEMS_NO_ASR | \
                                           RTEMS_INTERRUPT_LEVEL(0))

#define CONFIGURE_INIT
rtems_task Init (rtems_task_argument argument);

#include <rtems/confdefs.h>

