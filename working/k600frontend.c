/*
 * =====================================================================================
 *
 *       Filename:  k600frontend.c
 *
 *    Description:  new version based on k600frontend.c written by Sean Murray, tlabs.
 *
 *        Version:  2.0
 *        Created:  08/05/2009 10:53:15
 *       Revision:  revised version for version 1.
 *       
 *       Compiler:  gcc
 *       Linker  :  ld
 *       System  :  linux
 *
 *         Author:  Mr. Lee Pool (LCP), funnyvoice@tlabs.ac.za
 *        Company:  iThemba Laboratory for Accelerator Based Sciences
 *
 *	Changelog: 1.) Implemented a second thread ( in progress )
 *		   2.) Add debugging code for signal handling ( in progress )
 * =====================================================================================
 */
/* #####  C HEADER FILE INCLUDES   ######################################## */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <assert.h>
#include <uart.h>

/* #####   MIDAS HEADER FILE INCLUDES   ################################### */
#include "midas.h"
#include "mcstd.h"
//#include "/opt/vmecrates/concurrentvme/experim.h"
#include "experim.h"
#include "mvmestd.h"
#include "msystem.h"


/* #####   MODULES HEADER FILE INCLUDES  ################################## */
#include "v1190A.h"	// TDC
#include "v830.h"
#include "v259.h"	
#include "v792.h"	//QDC

#include "rtemsnetwork.h"
#include "rtemsVME.h"
#include <rtems/rtems/timer.h>
#include <rtems/confdefs.h>



/* #####   MACROS  -  LOCAL TO THIS SOURCE FILE  ########################## */
/* make frontend functions callable from the C framework */
#ifdef __cplusplus
	extern "C" {
#endif

/*-----------------------------------------------------------------------------
 *    Debug and Signal handlers
 *-----------------------------------------------------------------------------*/
//#define DEBUG 1
//#define DEBUG2 1
//#define DEBUGMESSAGE 1
//#define DEBUGDATA 1
//#define HDL_SIGNALS	

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ###################### */

/* The frontend name (client name) as seen by other MIDAS clients   */
char *frontend_name = "K600frontend";

/* The frontend file name, don't change it */
char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 3000;

/* maximum event size produced by this frontend */
INT max_event_size = 1000000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 10 * 1000000;

/*-----------------------------------------------------------------------------
 *  VME Hardware Specific Variables 
 *-----------------------------------------------------------------------------*/
MVME_INTERFACE *k600vme;

int V792_BASE;
int V259_BASE;
int V1190_BASE[7];
int V830_BASE[2];
int MaxScalers=2;
const int V1190Count=2;//N_TDC_MOD;

/* number of channels */
#define N_ADC  16
//#define N_TDC  768
//#define N_TDC  896
#define MAXINPUTSCALERS 9
int N_TDC;

const unsigned int N_TDC_MOD= 7;
#define N_SCLR 64
#define N_PAT 1
	
const int kDataSize = 100000;
DWORD tdcdata[1000000];

DWORD  storedeventcounter[7];
DWORD  eventcounter[7];
DWORD qdceventcounter;
DWORD avgtdctime,avgqdctime,avgpattime;
DWORD prevscalercount[3][MAXINPUTSCALERS];

char *tempbuffer;

//unsigned short Channel2Wire[N_TDC];
unsigned short *Channel2Wire;

extern HNDLE hDB;
HNDLE hSet;
WIRECHAMBER_SETTINGS K600TriggerSettings;


/* #####   PROTOTYPES  -  LOCAL TO THIS SOURCE FILE   ############################### */
/*-----------------------------------------------------------------------------
 *  Midas Frontend Function Prototypes
 *-----------------------------------------------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();

/*-----------------------------------------------------------------------------
 *  Equipment List Function Prototypes
 *-----------------------------------------------------------------------------*/
INT read_wirechamber_event(char *pevent, INT off);
INT read_scaler_event(char *pevent, INT off);

/*-----------------------------------------------------------------------------
 *  Module Read Out Function Prototypes
 *-----------------------------------------------------------------------------*/
int read_v259(const char*bank_name,char*pevent);
int read_tdc(char *pevent);
//int read_v792(int base,const char*bank_name,char*pevent,int number_of_channels);
int read_v792(int base, int number_of_channels, DWORD* returnData );

/*-----------------------------------------------------------------------------
 *  Miscelaneous functions
 *-----------------------------------------------------------------------------*/
int MapChannel2Wire(int channel_number);
void wirechamber_settings_callback(INT hDB, INT hseq, void *info);

/* #####   VARIABLES  -  LOCAL TO THIS SOURCE FILE   ################################ */

/*-----------------------------------------------------------------------------
 *    Equipment list 
 *-----------------------------------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {
   {"WireChamber",                /* equipment name */
    {1, 0,                   	  /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     EQ_POLLED,                   /* equipment type */
//     EQ_MULTITHREAD,
     LAM_SOURCE(0, 0xFFFFFF),     /* event source crate 0, all stations */
     "MIDAS",                     /* format */
     TRUE,                        /* enabled */
     RO_RUNNING |		  /* read only when running */
     RO_ODB,                      /* and update ODB */
     250,                         /* poll for 250ms */
     0,                           /* stop run after this event limit */
     0,                           /* number of sub events */
     0,                           /* don't log history */
     "concurrentvme", /* host on which front is running*/	
     "k600frontend", 		  /* frontend name*/
     "k600frontend.c",},  /* source file used for user FE  */	
    read_wirechamber_event,       /* readout routine*/
    },
#if 0
   {"Scaler",                     /* equipment name */
    {2, 0,                        /* event ID, trigger mask */
     "K600SYSTEMSCALER",          /* event buffer */
     EQ_PERIODIC | EQ_MANUAL_TRIG,/* equipment type */
     0,                           /* event source */
     "MIDAS",                     /* format */
     TRUE,                        /* enabled */
     RO_RUNNING | RO_TRANSITIONS |/* read when running and on transitions */
     RO_ODB,                      /* and update ODB */
     5000,                        /* read every 5 sec */
     0,                           /* stop run after this event limit */
     0,                           /* number of sub events */
     1,                           /* log history */
     "concurrentvme", /*host on which the front end is running*/
     "k600frontend",              /*front end name*/
     "k600frontend.c",}, /* source file used for user FE */
    read_scaler_event,            /* readout routine */
    },
#endif
   {""}
};

#ifdef __cplusplus
	}
#endif
volatile bool stopThread = 0;

volatile int pdataready = -1;

volatile bool Readout= 0;

volatile bool readoutcomplete = 0;

int ringbh1=0, ringbh2 = 0, ringbh1_next = 0, ringbh2_next = 0;

int ringbh_1190[7]; 


int f_mutex =0;


rtems_task Application_task(
  rtems_task_argument argument
)
{  
	char *pevent;
	int qdccounts = -1;
	int status;
	DWORD   *p;
	void   *wp;
	DWORD  *datareturned;
	int isDataReady = 0;
	while(stopThread){

		//printf("[task] 1 #### readout %d, isDataReady %d,  readoutcomplete %d \n",Readout,isDataReady,readoutcomplete );
		//fflush(stdout);
	
		if(Readout){

		/* polling */
		isDataReady = v792_DataReady(k600vme,V792_BASE);

		if(isDataReady){
		
				readoutcomplete = 0;
				
			
				qdccounts=read_v792(V792_BASE,V792_MAX_CHANNELS,datareturned );
	
				
      					
				status = rb_get_wp(ringbh1, &wp, 0);
				
				if( status == DB_TIMEOUT )
					continue;

				if( status != DB_SUCCESS ){
					printf("[task] 1.1 ######### no rb success \n ");
					fflush(stdout);
					break;
				}
	

				//wp = (DWORD*)datareturned;				
			/*        	
				int i = 0;
				for (i = 0; i < qdccounts; i++ ){
				//	printf("[task] data returned from readout %x \n",*(((DWORD*)datareturned) + (i)) );
					wp += *((datareturned) + (i)); 
				}
	                  */      
				
				//printf("[task] qdccounts %d \n",qdccounts );
				
				memcpy(wp,datareturned,qdccounts*sizeof(DWORD));				
				
				//rb_increment_wp(ringbh1, sizeof(p) );
				rb_increment_wp(ringbh1, qdccounts*sizeof(DWORD) );
				
				/*
				int buflevel = 0;
				status = rb_get_buffer_level(ringbh1,&buflevel);
				printf("[task] buffer level %d \n",buflevel );
	                        */
				
				readoutcomplete = 1;
			}// is Read Data enabled

	
		}// is there Data

        


		//printf("[task] 2 ####readout %d, isDataReady %d,  readoutcomplete %d \n",Readout,isDataReady,readoutcomplete );
	        //fflush(stdout);
	}

      	exit( 0 );
}



/* #################################################### */
extern INT mfe(char *ahost_name, char *aexp_name, BOOL adebug);


/*
 * RTEMS Startup Task
 */
rtems_task
Init (rtems_task_argument ignored)
{
  rtems_name        task_name;
  rtems_id          tid;
  rtems_status_code status;

  printk( "Initialize network\n" );
  rtems_bsdnet_initialize_network ();
  printk( "Network initialized\n" );
  rtems_bsdnet_show_inet_routes ();
 
  

//  int numberofdriver = libbsdport_netdriver_dump(NULL);
//
// 
//  printf(" [Task Init] Mem assigned to CONFIGURE_EXECUTIVE_RAM_SIZE = %d \n ",CONFIGURE_EXECUTIVE_RAM_SIZE );	
//  printf(" [Task Init] RTEMS_MINIMUM_STACK_SIZE = %d \n ",RTEMS_MINIMUM_STACK_SIZE );	
//
 
//
//  printf( "\n\n*** SAMPLE SINGLE PROCESSOR APPLICATION ***\n" );
//  printf( "Creating and starting an application task\n" );
//
//  task_name = rtems_build_name( 'T', 'A', '1', ' ' );
//
//  status = rtems_task_create( task_name, 100 , RTEMS_MINIMUM_STACK_SIZE ,  RTEMS_DEFAULT_MODES | RTEMS_PREEMPT | RTEMS_TIMESLICE , RTEMS_DEFAULT_ATTRIBUTES, &tid );
//
  
//  status = rtems_task_start( tid, Application_task, 0 );


//  stopThread = 1;

//  int i = 0;
//  while( i < 100){
//	  printf(" HELLO WORLD \n");
//	  i++;
 // }
    /* Start the Frontend */
  mfe("xiafe","k600test",0 );
  stopThread = 0;
 
 
 
  rtems_task_delete(RTEMS_SELF);
}


/* #####   FUNCTION DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ############ */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  frontend_init
 *  Description :   When the frontend program is started. 
 *  		    This routine should initialize the hardware.
 *  Parameters  :   Void
 *  Returns     :   SUCCESS,FE_ERR_HW
 * =====================================================================================
 */
INT frontend_init()
{
	int status=0;
	int counter=0;
	char set_str[80];
	int size;
	//int tdcmod;
	//HNDLE hkey;
	
	//print message and return FE_ERR_HW if frontend should not be started 
	printf("Frontend Testing Access to ODB for setup procedures.\n");
	

	/* cm_msg(MINFO,"k600frontend","Frontend Testing Access to ODB for setup procedures."); */
		

	WIRECHAMBER_SETTINGS_STR(wirechamber_settings_str);

	//Map /equipment/Trigger/settings for k600 
  	sprintf(set_str, "/Equipment/WireChamber/Settings");
  	status = db_create_record(hDB, 0, set_str, strcomb(wirechamber_settings_str));
  	
	printf("\n status db_create_record = %d \n ",status);
	status = db_find_key (hDB, 0, set_str, &hSet);
  	if (status != DB_SUCCESS)
    		cm_msg(MINFO,"FE","Key %s not found", set_str);
	
       	//Enable hot-link on settings/ of the equipment
  	size = sizeof(WIRECHAMBER_SETTINGS);
  	if ((status = db_open_record(hDB, hSet, &K600TriggerSettings, size, MODE_READ, wirechamber_settings_callback, NULL)) != DB_SUCCESS){
		cm_msg(MERROR,"Frontend Init","unable to open data base record\n");
		cm_disconnect_experiment();
		return status;
  	}


//	cm_get_experiment_database(&hDB,NULL);
//	db_find_key(hDB,0,"/Equipment/WireChamber/Settings",&hkey);	
	//read Triggger settings
//  	size = sizeof(WIRECHAMBER_SETTINGS);
 // 	if ((status = db_get_record (hDB, hSet, &K600TriggerSettings, &size, 0)) != DB_SUCCESS){
//		cm_msg(MERROR,"ODB update failure"," failure to update online database");
///		return status;
  // 	}
	

  
	/* Base Address of Modules */
	V259_BASE	=	0xD0000;
	
	V792_BASE  	=	0xb0000;
//	V792_BASE  	=	0xe0000; 
	
	V830_BASE[0]	=	0xe0000;
	V830_BASE[1]	=	0xf0000;
	
//	V1190_BASE[0]	=	0xd0000;
	V1190_BASE[0]	=	0x40000;
	V1190_BASE[1]	=	0x50000;
	V1190_BASE[2]	=	0x60000;
	V1190_BASE[3]	=	0x70000;
	V1190_BASE[4]	=	0x80000;
	V1190_BASE[5]	=	0x90000;
	V1190_BASE[6]	=	0xa0000;


	//assert(&K600TriggerSettings == NULL );
/*
	V1190_BASE[0]	=	K600TriggerSettings.v1190a_0.baseaddress;
	V1190_BASE[1]	=	K600TriggerSettings.v1190a_1.baseaddress;
	V1190_BASE[2]	=	K600TriggerSettings.v1190a_2.baseaddress;
	V1190_BASE[3]	=	K600TriggerSettings.v1190a_3.baseaddress;
	V1190_BASE[4]	=	K600TriggerSettings.v1190a_4.baseaddress;
	V1190_BASE[5]	=	K600TriggerSettings.v1190a_5.baseaddress;
	V1190_BASE[6]	=	K600TriggerSettings.v1190a_6.baseaddress;
*/	
	printf("[init] v1190 base address 0 is 0x%x \n",V1190_BASE[0] );

	printf("[init] v1190 base address 1 is %d \n",V1190_BASE[1] );
	printf("[init] v1190 base address 2 is %d \n",V1190_BASE[2] );
	printf("[init] v1190 base address 3 is %d \n",V1190_BASE[3] );
	printf("[init] v1190 base address 4 is %d \n",V1190_BASE[4] );
	printf("[init] v1190 base address 5 is %d \n",V1190_BASE[5] );

	printf("[init] v1190 base address 6 is %d \n",V1190_BASE[6] );

	N_TDC = K600TriggerSettings.chambers.global.max_tdc_channels;
/*
	for(tdcmod=0;tdcmod<V1190Count;tdcmod++){
		if(V1190_BASE[tdcmod]==0) {
			cm_msg(MERROR,"k600frontend init","EISH BANTWANA !! Zero base address for tdc module %d",tdcmod);
			if(tdcmod==6) V1190_BASE[tdcmod]=0xb0000;
			else V1190_BASE[tdcmod] = 0x40000+(tdcmod<<16);
			cm_msg(MERROR,"k600frontend init","EISH BANTWANA !! Setting module %d base address to default of 0x%08x",tdcmod,V1190_BASE[tdcmod]);
			printf("Setting module %d to have base address of : 0x%08x\n",tdcmod, V1190_BASE[tdcmod]);
		}
	}
	printf("[init] max tdc channels %d \n",N_TDC);
*/
	//create vme window
	//system("clear");
	printf(" # $ Frontend Hardware Initiliasation Of VME Window.... $ #\n");
/*	cm_msg(MINFO,"k600 frontend","Frontend Hardware Initiliasation Of VME Window");  */


	//create vme ring buffer
	if(!ringbh1){
		status = rb_create(event_buffer_size, max_event_size, &ringbh1 );
		if( status != DB_SUCCESS ) {
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}
	}

	//create polling ring buffer
	if(!ringbh2){
		status = rb_create(event_buffer_size, max_event_size, &ringbh2 );
		if( status != DB_SUCCESS ) {
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}
	}


	int tdc_module = 0;
	int current_ringb = 0;
	int prev_ringb = 0;
	for( tdc_module = 0; tdc_module < V1190Count; tdc_module++ ){
		status = rb_create(event_buffer_size, max_event_size, &current_ringb );
		if( status != DB_SUCCESS ) {
			printf("[frontend init] could not create rb \n");
			frontend_exit();		
		}

		if( current_ringb != prev_ringb )
			ringbh_1190[tdc_module] = current_ringb;
		else 
			continue;

	        prev_ringb = current_ringb;	

	}

  if (f_mutex == 0) {
      status = ss_mutex_create("readout", &f_mutex);
      if (status != SS_SUCCESS && status != SS_CREATED)
         cm_msg(MERROR, "mfe_set_error", "Cannot create mutex\n");
   }


	status = mvme_open(&k600vme,0);
	if (status!=1) { 
//		fprintf(stderr,"Exiting as status is %d\n",status);
		cm_msg(MERROR,"Frontend Init","Could Not open mvme device!");
		cm_disconnect_experiment();
		exit(1);
	}


	//sleep(1000);


	//setup V792N
	printk("Frontend Hardware Initializing V792N.....\n");
	v792_Setup(k600vme,V792_BASE,1);

//      	v792N_SetTestAcquisition(k600vme, V792_BASE,0xa0);

//	int cmode;
//	mvme_get_dmode(k600vme,&cmode);
  //	mvme_set_dmode(k600vme, MVME_DMODE_D16);
//	WORD value1 = (WORD)mvme_read_value(k600vme,V792_BASE + 0x1000);
//	mvme_set_dmode(k600vme,cmode);

//	printf(" \n\n\n\n v792  \t\t ==== version regs value  === %08x \n\n\n\n",value1 );

	//setup the 1190 tdc modules
	//fprintf(stdout,"Frontend Hardware Initializing V1190 TDC Modules.....\n");
	int setupmode = 1;
	for(counter=0;counter< V1190Count;counter++) {
		printf(" ########## TDC MODULE [%d] Initialization ########## \n", counter );
		v1190A_Setup(k600vme,V1190_BASE[counter],setupmode);
	}

	//reset the 259 module
	//nothing to do to setup this module.
	//fprintf(stdout,"Frontend Hardware Initializing V259 Nothing To Do! \n");

	//reset the scalers

	printk("Frontend Hardware Initializing V830 Scalers......\n");
	int scalercount;
  	for(scalercount=0;scalercount<MaxScalers;scalercount++){
		v830_Setup(k600vme,V830_BASE[scalercount],0x3);
		v82X_Status(k600vme,V830_BASE[scalercount] ); 

		v82X_SoftClear(k600vme,V830_BASE[scalercount]);
	}


  rtems_name        task_name;
  rtems_id          tid;
  rtems_status_code status2;



  task_name = rtems_build_name( 'T', 'A', '1', ' ' );
//
  status2 = rtems_task_create( task_name, 100 , RTEMS_MINIMUM_STACK_SIZE ,  RTEMS_DEFAULT_MODES | RTEMS_PREEMPT | RTEMS_TIMESLICE , RTEMS_DEFAULT_ATTRIBUTES, &tid );
//
  
  status2 = rtems_task_start( tid, Application_task, 0 );


  stopThread = 1;

 
	
//	fprintf(stdout,"\n # $ Frontend Hardware Initialisation Completed! $ # \n ");

//	sleep(100);
//	system("clear");
	

  // 	cm_msg(MINFO,"Frontend Init","finished testing access to online database for setup procedures.");
  	
//	system("clear");
	if(status!=1)return FE_ERR_HW;
   	else return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  frontend_exit
 *  Description :  When the frontend program is shut down. 
 *                 Can be used to releas any locked resources like memory, 
 *                 communications ports etc.
 *  Parameters  :  VOID
 *  Returns     :  SUCCESS, [ERROR]
 * =====================================================================================
 */
INT frontend_exit()
{

	ss_mutex_release(f_mutex);
	stopThread = 0;
    	mvme_close(k600vme);
   	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  begin_of_run
 *  Description : When a new run is started. Clear scalers, open rungates, etc.
 *  Parameters  : INT run_number
 *  		  char* error 
 *  Returns     : SUCCESS,[ERROR]
 * =====================================================================================
 */
INT begin_of_run(INT run_number, char *error)
{
	
	Readout = 1;

//	int counter=0;
  //	int i=0;
//  	int size;
//  	int status;
   // 	int csr;
/*
	//read Triggger settings
  	size = sizeof(WIRECHAMBER_SETTINGS);
  	if ((status = db_get_record (hDB, hSet, &K600TriggerSettings, &size, 0)) != DB_SUCCESS){
		cm_msg(MERROR,"ODB update failure"," failure to update online database");
		return status;
   	}
*/	
	//Setting Thresholds
#if 0
	WORD thold[V792N_MAX_CHANNELS];
  	for (i=0;i<V792N_MAX_CHANNELS;i++) {
/*		printf("T%i:0x%4.4x ,", i, K600TriggerSettings.v792n_0.thresholds[i]);
*/  
		thold[i] = K600TriggerSettings.v792n_0.thresholds[i];
  	}
	v792N_ThresholdWrite(k600vme, V792_BASE, thold );

	//Clearing v792N and checking stat reg
	v792N_DataClear(k600vme, V792_BASE);
  	csr = v792N_CSR1Read(k600vme, V792_BASE);
 	printf("Data Ready ADC0: 0x%08x\n", csr);

	// clear QDC
  	v792N_EvtCntReset(k600vme,V792_BASE);
  	v792N_SoftReset(k600vme,V792_BASE);

	// clear TDCS 
  	for(counter=0;counter<V1190Count;counter++) {
		v1190A_SoftClear(k600vme,V1190_BASE[counter]); 
  	}
	
	// clear scalers
	for(counter=0;counter<MaxScalers;counter++) {
		v82X_SoftClear(k600vme,V830_BASE[counter]);
	}
	memset(prevscalercount,0,18*sizeof(DWORD));
#endif

	// clear pattern register.
	//printf(" ###### $$$$$$$ SUCCESSFULLY PASSED BEGIN OF RUN  $$$$$$$ #######\n");

	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  end_of_run
 *  Description :  Called on a request to stop a run. 
 *  		   Can send end-of-run event and close run gates.
 *  Parameters  :  INT run_number
 *  		   char* error 
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT end_of_run(INT run_number, char *error)
{
	Readout = 0;
	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  pause_run
 *  Description :  When a run is paused. Should disable trigger events. 
 *  Parameters  :  INT run_number
 *  		   char* error 
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT pause_run(INT run_number, char *error)
{
	//stop scalers
	//clear tdc's
	int scalercount;
   	for(scalercount=0;scalercount<MaxScalers;scalercount++)
		v82X_SoftClear(k600vme,V830_BASE[scalercount]);
   
	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  resume_run
 *  Description :  When a run is resumed. Should enable trigger events.
 *  Parameters  :  INT run_number
 *  		   char* error
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT resume_run(INT run_number, char *error)
{
	//restarsaadq scalers
	//clear tdc's
	int scalercount;
   	for(scalercount=0;scalercount<MaxScalers;scalercount++)
		v82X_SoftClear(k600vme,V830_BASE[scalercount]);
   
	return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  frontend_loop
 *  Description :   if frontend_call_loop is TRUE, this routine gets called, 
 *  		    when the frontend is idle or once between every event.
 *  Parameters  :  VOID
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT frontend_loop()
{
   return SUCCESS;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  poll_event
 *  Description :  Polling routine for events. Returns TRUE if event is available. 
 *                 If test equals TRUE, don't return. 
 *                 The testflag is used to time the polling
 *  Parameters  :  INT source
 *  		   INT count
 *  		   BOOL test
 *  Returns     :  SUCCESS,[ERROR]
 * =====================================================================================
 */
INT poll_event(INT source, INT count, BOOL test)
{
	int i;
  	int DataReady = 0;
	void *rp;
	void *wp;
	int status;
	int nlevel = 0;

//	printf("\n [poll_event] count = %d \n",count );
//	fflush(stdout);
//	cm_yield(2);


	//printf(" [poll_event] k600vme %p \n",k600vme );
	//fflush(stdout);


	for(i = 0; i < count; i++, DataReady++ ) {
	
		//printf("[poll_event] setting mutex \n");	
		//ss_mutex_wait_for(f_mutex,0);
		//status = rb_get_wp(ringbh1, &wp, 0);
		//ss_mutex_release(f_mutex);
		//status = rb_get_rp(ringbh1, &rp, 0);

		 rb_get_buffer_level(ringbh1,&nlevel);
   
		if( nlevel > 0 ){
			if(!test){
			     //DataReady = (int)rp;
			     //printf("[poll_event] DataReady %8x \n",DataReady ); 
			     //status = rb_increment_rp(ringbh1, sizeof(int));
			     return 1;
			}
		}
	}





/*	
	for (i = 0; i < count; i++, DataReady++) {
		//printf(" [poll_event] k600vme %p \n",k600vme );
		//fflush(stdout);
    		pdataready = 0;
		DataReady = v792N_DataReady(k600vme,V792_BASE);
		//DataReady = 1;
  		//printf(" [poll_event] DataReady = %x \n",DataReady ); 
  		if (DataReady){
			if (!test){
			    pdataready = DataReady;
			    return DataReady;
			}
  		 }


  	}

*/
/*	printk("Data not ready\n"); */
	
       	return 0;
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  interrupt_configure
 *  Description :  
 *  Parameters  :  
 *  Returns     :  
 * =====================================================================================
 */
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
	/* REMAINS TO BE IMPLEMENTED */
   	switch (cmd) {
		case CMD_INTERRUPT_ENABLE:
			break;
		case CMD_INTERRUPT_DISABLE:
			break;
   		case CMD_INTERRUPT_ATTACH:
      			break;
   		case CMD_INTERRUPT_DETACH:
      			break;
  	}

	return SUCCESS;
}

/* #####   Event Readout Routines FUNCTION DEFINITIONS  ################### */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_v259
 *  Description :  Event Readout for the v259 Module
 *  Parameters  :  const char* 		bank_name
 *  		   char*		pevent
 *  Returns     :  wcount,[ERROR]
 * =====================================================================================
 */
int read_v259( const char*bank_name , char*pevent )
{
	int wcount = 0;
	DWORD data;
 	DWORD* pdata;
  	int cmode;
	
	// create PAT Register bank 
	bk_create(pevent, bank_name, TID_DWORD, &pdata);
	
	// read data and increment pointer
	mvme_get_am(k600vme,&cmode);
	mvme_set_am(k600vme, MVME_AM_A24_ND);
	data=v259_ReadPatternReset(k600vme,V259_BASE);
	mvme_set_am(k600vme,cmode );
	
#ifdef DEBUG
	printf("V259 : pattern : 0x%04x\n",data);
#endif
	
	pdata[0]=data;
	pdata+=sizeof(DWORD);
	wcount++;
	// close PAT Register bank
  	bk_close(pevent, pdata);
	
  	return wcount;
}







/* -------------------- READOUT THREAD TASK ----------------------------- */



/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_v792
 *  Description :  Event Readout for the v792 Module
 *  Parameters  :  int			base
 *  	           const char*		bank_name
 *  	           char*		pevent
 *  	           int			number_of_channels
 *  Returns     :  wcount,[ERROR]
 * =====================================================================================
 */
int read_v792(int base, int number_of_channels, DWORD* returnData )
{
	int i;
  	int wcount = 0;
  	DWORD data[128];
	
	DWORD *ppdata = NULL;

  	unsigned int counter = 0;
	DWORD* pdata;
	unsigned int w;
	int chan,val;
	int valid[number_of_channels];
	int cmode;
	int rstatus;
	int nentries = 128;

//	DWORD qdceventcount;
	
//        printf("[read_v792] ------------------- READ V792 VME ============= \n");	
  	/* Event counter */
  	v792_EvtCntRead(k600vme, base, &counter);
  	//printf("792 counter = %d \n",counter);
	
#ifdef DEBUGC
	v792N_EvtCntRead(k600vme, V792_BASE, &qdceventcount);
	if(qdceventcount!=qdceventcounter+1)
		cm_msg(MINFO,"792 readout","event counter mismatch : %u!=%u+1",(unsigned int)qdceventcount,(unsigned int)qdceventcounter);
	qdceventcounter=qdceventcount;
#endif	

	/*  Single Event Read */
	//mvme_get_am(k600vme,&cmode);
  	//mvme_set_am(k600vme, MVME_AM_A32_ND);
 	//v792_EventRead(k600vme, base, data, &wcount);
  	//mvme_set_am(k600vme,cmode );

	

	mvme_get_am(k600vme,&cmode );
	mvme_set_am(k600vme, MVME_AM_A32_NB );
	mvme_set_dmode(k600vme, MVME_DMODE_D32 );
	mvme_set_blt(k600vme,MVME_BLT_BLT32);
        int number_of_entries = v792_DataRead(k600vme,base, ppdata,&nentries );
        mvme_set_am(k600vme,cmode );

  	/* create QDCS bank */
  	//bk_create(pevent, bank_name, TID_DWORD, &pdata);

  	for (i=0; i<number_of_channels; i++){
		pdata[i] = 0;
    		valid[i]=0;
  	}

	wcount = number_of_entries;

  	for (i=0; i<wcount; i++)
  	{
      		//w = data[i];
		w = *((DWORD*)ppdata + i );  
      		//w = w + i;
		//printf(" [%d] w  = 0x%.08x \n ",i,w );
      		if (((w>>24)&0x7) != 0) continue;
      		chan = (w>>17)&0x1F;
      		val  = (w&0x0FFF);
      		//pdata[chan] = w;
      		*pdata++ = w;
		//	valid[chan]=1;
		//	if(((w>>12)&0x1)!=0){ pdata[chan]=0;valid[chan]=0;printf("overflow on chan : %d 0x%08x\n",chan,data[i]);} // overflow
		//	if(((w>>13)&0x1)!=0){ pdata[chan]=0; valid[chan]=0;printf("underflow on chan: %d 0x%08x\n",chan,data[i]);}// underflow
   	}	


#ifdef DEBUG 
	printf("counter %6d, words: %3d, header: 0x%08x, ADC0: 0x%08x, ADC0,1,2: %6d %6d %6d\n",
	       counter,wcount,data[0],pdata[0],pdata[0],pdata[1],pdata[2]);
	printf("counter %6d, words: %3d, header: 0x%08x,",
	       counter,wcount,data[0]);
	for(i=0;i<16;i++)printf("%d-%d:0x%04x ",i,valid[i],pdata[i]);
		 printf("\n");
#endif
		 
//  	for(i=0;i<number_of_channels;i++)pdata++;  //pdata += number_of_channels;//*sizeof(DWORD);

//  	bk_close(pevent, pdata);
 	
//       	printf("[read_v792] -------END---------- READ V792 VME ============= \n");	
  
 	returnData = ppdata;	
	return wcount;
}


/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_tdc
 *  Description :  Event Readout for TDC (v1190) Modules
 *  Parameters  :  char*		pevent
 *  Returns     :  [0],[ERROR]
 * =====================================================================================
 */
int read_tdc(char *pevent)
{
	int count;
	int wire=0;
	int geo;
	DWORD* pdata32;
	int edge,channel,time,code;
	int i;
//	int xdata;
	int counts=200;
	int totalcounts=0;	// number of words
	int tdcmodulecounter=0;
	int wordcount[7];
	int datacount[7];

#ifdef DEBUGC	
	int cmode;
	DWORD diffcounter = 0;
	DWORD prevcounter = 0;
	DWORD prevevcounter = 0;
	DWORD eventdiffcounter = 0;
#endif
	/* create TOPA bank */
	bk_create(pevent, "TDC0", TID_DWORD, &pdata32);

	// Per TDC
	for(tdcmodulecounter=0;tdcmodulecounter<V1190Count;tdcmodulecounter++){
  		
//		memset(tdcdata,kDataSize,sizeof(DWORD));
//		memset(tdcdata,'\0',sizeof(kDataSize));
		
		geo=0;
	  	wordcount[tdcmodulecounter]=0;
		datacount[tdcmodulecounter]=0;
		
#ifdef DEBUGC
		prevcounter =  storedeventcounter[tdcmodulecounter];
		prevevcounter =  eventcounter[tdcmodulecounter];
	//	mvme_get_dmode(k600vme,&cmode);
  	//	mvme_set_dmode(k600vme, MVME_DMODE_D16);
  		storedeventcounter[tdcmodulecounter]  = v1190A_EvtStored(k600vme,V1190_BASE[tdcmodulecounter]);//mvme_read_value(k600vme, V1190_BASE[tdcmodulecounter]+0x1020);
		eventcounter[tdcmodulecounter]=v1190A_EvtCounter(k600vme,V1190_BASE[tdcmodulecounter]);
	//	mvme_set_dmode(k600vme,cmode );

//		diffcounter =  abs( storedeventcounter[tdcmodulecounter] - prevcounter );
		diffcounter =  abs( storedeventcounter[tdcmodulecounter] - prevcounter );
		eventdiffcounter=eventcounter[tdcmodulecounter]-prevevcounter;
		if( diffcounter > 1 ){
			cm_msg(MINFO,"Read_TDC","DataSkipped in mod %d diffcounter %d", tdcmodulecounter,diffcounter );
		}
		if(eventdiffcounter!=1){
			cm_msg(MINFO,"Read_TDC","DataSkipped eventcount in mod %d : %d != %d +1 ", tdcmodulecounter,eventcounter[tdcmodulecounter],prevevcounter);
		}
		
#endif

		/* Single Event Read */
		count = v1190A_EventRead(k600vme, V1190_BASE[tdcmodulecounter], tdcdata, &counts);  //Single event read
		
		
		//assert(data != NULL );
		
		//printf("[1] TDC count ---> <%d> , [2] TDC counts ---> <%d> \n",count,counts );

		if (counts > 0){ // we have data
//			counts=127; // TODO !!!!!!!! REMOVE THIS ITS DUE TO DataRead returning the number of words read which define to be 200 due to ensuring double wire firings.
#ifdef DEBUG
			if (counts > 127)
				printf("reading TDC got %d words\n",counts);
#endif
#ifdef DEBUG2
			printf("reading TDCmodule[0x%08x]  %d got %d words\n",V1190_BASE[tdcmodulecounter],tdcmodulecounter,counts);
#endif
			*pdata32++=tdcmodulecounter+(0x1f<<27);

			wordcount[tdcmodulecounter]=counts;
			for (i=0; i<counts; i++) {
				code = 0x1F&(tdcdata[i]>>27);

				if (tdcdata[i] == 0) continue;

				switch (code) {
					case 0: // valid measurent word 
						edge = 0x1&(tdcdata[i]>>26);
						channel = 0x7F&(tdcdata[i]>>19);
						time = 0x3FFFF&tdcdata[i];
						if(tdcmodulecounter==0){
							if(channel<2){
								wire=600+channel;
							}
//							switch(channel){
//								case 0 : printf("reftime : %d at wire %d\n",time,wire);break;
//								case 1 : printf("tof time : %d at wire %d\n",time,wire);break;
//							}
						}
//						xdata=time+((wire<<19)); //data can contain 19 bits of info, map the wire above that.
						*pdata32++ = tdcdata[i];
						datacount[tdcmodulecounter]++;
						#ifdef DEBUGDATA
 					  	printf("module : %d tdc %3d: 0x%08x, code 0x%02x, edge %d, chan %2d,time %6d\n", tdcmodulecounter, i, tdcdata[i], code, edge, channel, time);
						#endif
//						if ((channel==0) && (time0==0)) time0 = time;
//						if ((channel==1) && (time0!=0) && (time1==0) && (time>time0)) time1 = time;
//						if ((channel==2) && (time0!=0) && (time2==0) && (time>time0)) time2 = time;
        					break; // end of case 0
				}// switch statement
			} // for i
			totalcounts+=counts;
		} //count >0
#ifdef DEBUG2
		else		printf("count %d, we have no data\n",count);
#endif

		}//for tdcmodulecounter

  		bk_close(pevent, pdata32);

		//clear event buffer in module. Keeps the settings loaded into the modules.
		for(tdcmodulecounter=0;tdcmodulecounter<V1190Count;tdcmodulecounter++){
			v1190A_SoftClear(k600vme,V1190_BASE[tdcmodulecounter]);
		}


  		return totalcounts;
}

/* #####   FUNCTION DEFINITIONS - Misc. Functions -  ##################### */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  wirechamber_settings_callback
 *  Description :  Misc Func. Call Back for ODB
 *  Parameters  :  INT		hDB
 *  		   INT		hseq
 *  		   void*	info
 *  Returns     :  VOID 
 * =====================================================================================
 */
void wirechamber_settings_callback(INT hDB, INT hseq, void *info)
{
/*	
 	char set_str[128];
	int status=0;
	HNDLE hKey,key,hsubkey;
		
	printf("odb ... wirechamber settings touched with hseq : %d \n",hseq);
	cm_msg(MINFO,"WireChamberSettingsCallback","Settings for wirechamber have changed\n");
	sprintf(set_str, "/Equipment/WireChamber/Settings");
	status = db_find_key (hDB, 0, set_str, &hKey);
	db_copy_xml(hDB,hKey,xml_buffer,&xml_buffer_size);

	status= db_save_xml(hDB,hSet,"k600settings.xml");
	switch(status){
		case DB_SUCCESS : printf("successfully written xml to file : k600settings.xml\n");break;
		case DB_FILE_ERROR : printf("DB_FILE_ERROR on writing xml\n");break;
	}
*/
}

/* #####   FUNCTION DEFINITIONS  -  WireChamber and Scaler Functions   ##################### */

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_wirechamber_event
 *  Description :   This involved reading you the v1990's, 
 *  		    v792, v259 tdcs, qdc, hitpattern, respectively
 *  Parameters  :   char*	pevent
 *  		    INT		off
 *  Returns     :   INT (bk_size(pevent))
 * =====================================================================================
 */
INT read_wirechamber_event(char *pevent, INT off)
{
	DWORD *pdata;
	EVENT_HEADER *pp;
   	//DWORD EventCounter;
  	//int tdccount=0;
   	//int tdccounta=0;
  	//int qdccount=0;
  	//int tdcmodulecounter=0;
   	int nbanks;
//   	int cmode;
   	char banklist[STRING_BANKLIST_MAX];
//	int tdccounts=-1;
	int qdccounts=-1;
//	int patcounts=-1;
	int status;

	//printf("\n [read_wirechamber_event] thread id [%d] \n",rtems_task_self());
	/* init bank structure */
	//bk_init32(pevent); for a bank of >32k events
	void *p;
	DWORD *prn;
	

//if(readoutcomplete) {

	bk_init(pevent);


	/* create QDCS bank */
  	bk_create(pevent,"ADC0" , TID_DWORD, &pdata);
	
	pp = (EVENT_HEADER*)pevent;
	//ss_mutex_wait_for(f_mutex,0);
	status = rb_get_rp(ringbh1, &p, 0);
	//ss_mutex_release(f_mutex);

//	prn = (DWORD*)p;

	int i = 0;
	for (i = 0; i < 128 ; i++ )
		printf("[%d] %08x \n",i,*((DWORD*)p + i ) );

	printf("\n");

	memcpy(pdata,p,128*sizeof(DWORD) );	
	
	status = rb_increment_rp(ringbh1,128*sizeof(DWORD));	 
/*
	printf("[read_wirechamber_event] pevent->event_id %d \n",pp->event_id );
	printf("[read_wirechamber_event] pevent->actual_time %d \n",pp->time_stamp );
	printf("[read_wirechamber_event] pevent->serial %d \n",pp->serial_number );
	printf("[read_wirechamber_event] pevent->data_size %d \n",pp->data_size );
	printf("[read_wirechamber_event] pdata --> %8x\n",pdata );
*/
	pdata += 128;
	bk_close(pevent,pdata);

	//printf("[wirechamber_event] Event  : %x \n",pevent);
//	printf("[event] @##############################################$@#$@#$@#$ \n" );

//    if(bk_size(pevent) < 152 ){	
/*
	printf("[wirechamber_event] ring buffer handle  : %d \n",ringbh1 );
	
	DWORD *testlist;
	int n_qdc;

	n_qdc = bk_locate(pevent, "ADC0", &testlist );
	int k = 0;
	for( k = 0; k < n_qdc; k++ )
		printf(" %x ",*(testlist + k ) );
	printf("\n");

	for(k = 0; k < 34; k++ )
		printf(" [%x] ",*(prn + k ) );
	printf("\n");

	
	nbanks=bk_list(pevent,banklist);
	printf("[wirechamber_event] #banks:%d List:%s\n", nbanks, banklist);
*/
//    }

//	printf("[event] @##############################################$@#$@#$@#$ \n" );




//}

	//nbanks=bk_list(pevent,banklist);

	// DRDY of v792 is only TRUE after conversion has taken place
	// we are there for now ready for readout, no waiting required.
	//Read QDC

		
	/* qdccounts=read_v792(V792_BASE,"ADC0",pevent,V792N_MAX_CHANNELS); */
	
	/*	
		if( status == DB_SUCCESS ){
		
			if( sizeof(pevent) < max_event_size  && sizeof(pevent) > 0 ) {
				//printf("[wirechamber_event] Event  : %x \n",pevent);
				//printf("[wirechamber_event] Event size is : %d\n",bk_size(pevent));
				nbanks=bk_list(pevent,banklist);
				//printf("[wirechamber_event] #banks:%d List:%s\n", nbanks, banklist);
			}
		}else
			printf("[wirechamber] get readpointer failed \n");
        */
			











//	printf("After qdc bank close #banks:%d List:%s\n",nbanks,banklist);
//	fflush(stdout);
	//Read TDC's
/*
	tdccounts=read_tdc(pevent);
	nbanks=bk_list(pevent,banklist);
	printf("Event size is ---->  %d\n",bk_size(pevent));
	printf("After tdc bank close #banks:%d List:%s\n",nbanks,banklist);
*/
	//Read Pattern reg

/*	
	mvme_get_am(k600vme,&cmode);
	mvme_set_am(k600vme, MVME_AM_A24_ND);
	patcounts=read_v259("PAT0",pevent);
	nbanks=bk_list(pevent,banklist);
	mvme_set_am(k600vme,cmode);
*/

	//printf("[1]Event size is : %d\n",bk_size(pevent));
	//printf("[2]After pattern register bank close #banks:%d List:%s\n",nbanks,banklist);

	//for safety sake clear and reset the qdc as this is fundamental to achieving a good trigger.

	//printf("Event size is : %d\n",bk_size(pevent));
	//printf("Wire chamber event finished returning bank size %d!!!\n",bk_size(pevent));

#ifdef DEBUG2
	printf("Exiting Wirechamber readout\n");
	fflush(stdout);
#endif
#ifdef DEBUGMESSAGE
	cm_msg(MINFO,"datacounts"," qdc : %d tdc : %d pat : %d\n",qdccounts,tdccounts,patcounts);
#endif 
  	return bk_size(pevent);
}

/* 
 * ===  FUNCTION  ======================================================================
 *  Name        :  read_scalar_event
 *  Description :  Read out the scaler events from the V830.
 *  Parameters  :  char* 	pevent
 *  		   INT		off
 *  Returns     :  INT		(bk_size(pevent))
 * =====================================================================================
 */
INT read_scaler_event(char *pevent, INT off)
{
   DWORD *pdata;
   DWORD scalerchannel=0,scalercount=0,scalercounter=0;
   //unsigned int ChannelEnable=0;
   unsigned int ChannelBit=1;
   /* init bank structure */
   bk_init(pevent);

   /* create SCLR bank */
   bk_create(pevent, "SCLR", TID_DWORD, &pdata);
   for(scalercount=0;scalercount<MaxScalers;scalercount++){
	ChannelBit=1;	
//	ChannelEnable=v830_ReadChannelEnalbe(k600vme,V830_BASE[scalercount]);
   	for(scalerchannel=0;scalerchannel<MAXINPUTSCALERS;scalerchannel++){
	       //cm_msg(MINFO,"Scalerreadout","reading scaler %d, at 0x%08x at channel %d",scalercount,V830_BASE[scalercount],scalerchannel);	
	       //printf("reading scaler %d, at 0x%08x at channel %d",scalercount,V830_BASE[scalercount],scalerchannel);	
		scalercounter=v82X_ReadCounter(k600vme,V830_BASE[scalercount],scalerchannel);
		*pdata++=scalercounter-prevscalercount[scalercount][scalerchannel];
     	         prevscalercount[scalercount][scalerchannel]=scalercounter;
	}
   }
   bk_close(pevent, pdata);
#ifdef TIMING
   cm_msg(MINFO,"readouttiming","qdc :%d us, tdc : %d us, pat: %d us",avgqdctime,avgtdctime,avgpattime);
#endif
   return bk_size(pevent);
}

