#include <stdio.h>
#include <assert.h>

#include "v830.h"

int v830_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry)
{
    int cmode, status;
    DWORD data_get;

    mvme_get_dmode(mvme, &cmode); 
    mvme_set_dmode(mvme, MVME_DMODE_D32);
    
    status = mvme_read(mvme, pdest, base, sizeof(DWORD) * nentry); 
/*    data_get = mvme_read_value(mvme, base ); */
    pdest = &data_get;

    mvme_set_dmode(mvme, cmode);
    return nentry;
    
}

/**
 * Read and display the curent status of the module.
 * @param *mvme VME structure
 * @param  base Module base address
 * @return MVME_SUCCESS
 */

int  v82X_Status(MVME_INTERFACE *mvme, DWORD base) {
	int   cmode;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	//-------------------------------------------------
	printf("\n--- Control Register:\n");
	int csr = mvme_read_value(mvme, base+V82X_CR_RW);
	printf("  CSR                      : 0x%04x\n", csr);
	printf("  CSR ACQ MODE             : 0x%04x\n", csr&0x3);
	printf("  CSR HEADER ENA           : 0x%04x\n", csr&0x32);
	
	printf("\n--- Status Register:\n");
	int sta = mvme_read_value(mvme, base+V82X_ST_RO);
	printf("  STA                      : 0x%02x\n", sta&0xff);
	printf("  DREADY                   : 0x%02x\n", sta&0x01);
	printf("  ALMOST FULL              : 0x%02x\n", sta&0x02);
	printf("  FULL                     : 0x%02x\n", sta&0x04);
	printf("  GLOBAL DREADY            : 0x%02x\n", sta&0x08);
	printf("  GLOBAL BUSY              : 0x%02x\n", sta&0x10);
	printf("  TERM ON                  : 0x%02x\n", sta&0x20);
	printf("  TERM OFF                 : 0x%02x\n", sta&0x40);
	printf("  BERR                     : 0x%02x\n", sta&0x80);
	
	printf("\n--- Interrupt:\n");
	int v = mvme_read_value(mvme, base+V82X_IRL_RW);
	printf("  ILR                      : 0x%02x\n", v&0x7);
	
	
	//-------------------------------------------------
	
	mvme_set_dmode(mvme, cmode);
	return 0;
}
		
/*****************************************************************/
void v82X_SoftReset(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_MODULE_RESET_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v82X_SoftClear(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
 
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_SOFT_CLEAR_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/**
 * Set Acquisition Mode to Trigger Random
 */
void v82X_SetTriggerRandom(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_CR_SET_WO, 0x1);
  mvme_write_value(mvme, base+V82X_CR_CLEAR_WO, 0x2);
  mvme_set_dmode(mvme, cmode);
}

/**
 * Set Acquisition Mode to Trigger Disabled
 */
void v82X_SetTriggerDisabled(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_CR_CLEAR_WO, 0x1);
  mvme_write_value(mvme, base+V82X_CR_CLEAR_WO, 0x2);
  mvme_set_dmode(mvme, cmode);
}

/*****************************************************************/
void v82X_SoftTrigger(MVME_INTERFACE *mvme, DWORD base)
{
  int cmode;
  mvme_get_dmode(mvme, &cmode);
  mvme_set_dmode(mvme, MVME_DMODE_D16);
  mvme_write_value(mvme, base+V82X_SOFT_TRIGGER_WO, 0);
  mvme_set_dmode(mvme, cmode);
}

/**
 * Read one Counter
 * @param *mvme VME structure
 * @param  base Module base address
 * @param n counter number (0-31)
 */
int v82X_ReadCounter(MVME_INTERFACE *mvme, DWORD base, int n)
{
	int cmode;
	DWORD v;
	assert( n>=0 && n<32);
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D32);
	v=mvme_read_value(mvme, base+V82X_COUNTER_BASE_RO+n*4);
	mvme_set_dmode(mvme, cmode);
	return v;
}
/**
 * Read Trigger Counter
 * @param *mvme VME structure
 * @param  base Module base address
 */
int v830_ReadTriggerCounter(MVME_INTERFACE *mvme, DWORD base)
{
	int cmode;
	DWORD v;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D32);
	v=mvme_read_value(mvme, base+V830_TRIG_CNT_RO);
	mvme_set_dmode(mvme, cmode);
	return v;
}
/**
 * Read Event Number
 * @param *mvme VME structure
 * @param  base Module base address
 */
int v830_EvtCounter(MVME_INTERFACE *mvme, DWORD base)
{
	int cmode;
	WORD v;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	v=mvme_read_value(mvme, base+V830_EVT_CNT_RO);
	mvme_set_dmode(mvme, cmode);
	return v;
}

int v830_Setup(MVME_INTERFACE *mvme, DWORD base, int mode)
{
	int cmode;
	WORD v;
	mvme_get_dmode(mvme, &cmode);
	mvme_set_dmode(mvme, MVME_DMODE_D16);
	switch(mode){
		case 0x0://random trigger
			break;
		case 0x1:// periodic trigger
				//set period trigger of 1sec
				mvme_write_value(mvme,base+V82X_CR_RW,0x0); // reset control register to zero 
				mvme_write_value(mvme,base+V82X_CR_SET_WO,0x2); // set acq mode to periodic trigger (internal) 
//zero is 32 bit				//mvme_write_value(mvme,base+V82X_CR_SET_WO,0x4); // set data format to 26 bit scaler 
			//	mvme_write_value(mvme,base+V82X_CR_SET_WO,0x20); // set header enable 
				mvme_set_dmode(mvme,MVME_DMODE_D32);
				mvme_write_value(mvme,base+V82X_DWELL_TIME_RW,0x2625A0); // set to 2,500,000 in units of 400ns (1sec)
			// set only the lowest 4 channels to be read out.
				mvme_write_value(mvme,base+V830_CHANNEL_ENABLE_RW,0xf);
				mvme_set_dmode(mvme,MVME_DMODE_D16);
			break;
		case 0x2:// interupt periodic trigger
			break;
		case 0x3:// periodic trigger of 10sec
				//set period trigger
				mvme_write_value(mvme,base+V82X_CR_RW,0x0); // reset control register to zero 
				mvme_write_value(mvme,base+V82X_CR_SET_WO,0x2); // set acq mode to periodic trigger (internal) 
				mvme_write_value(mvme,base+V82X_CR_SET_WO,0x20); // set header enable 
				mvme_write_value(mvme,base+V82X_CR_SET_WO,0x4); // set data format to 26 bit scaler 
				mvme_set_dmode(mvme,MVME_DMODE_D32);
				mvme_write_value(mvme,base+V82X_DWELL_TIME_RW,0x3d090); // set to 250,000 in units of 400ns (10sec)
			// set only the lowest 4 channels to be read out.
				mvme_write_value(mvme,base+V830_CHANNEL_ENABLE_RW,0xf);
				mvme_set_dmode(mvme,MVME_DMODE_D16);
			break;
		case 0x4:// periodic trigger
				//set period trigger of 1sec
				mvme_write_value(mvme,base+V82X_CR_RW,0x0); // reset control register to zero 
			// set only the lowest 12 channels to be read out.
				mvme_write_value(mvme,base+V830_CHANNEL_ENABLE_RW,0xffff);
				mvme_set_dmode(mvme,MVME_DMODE_D16);
			break;
	}	
	mvme_set_dmode(mvme, cmode);
	return v;
}
/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * c-file-style: "stroustrup"
 * tab-width: 4
 * End:
 */
